FROM openjdk:11
ADD target/docker-component-profile.jar docker-component-profile.jar
EXPOSE 7021
ENTRYPOINT ["java", "-jar", "docker-component-profile.jar"]