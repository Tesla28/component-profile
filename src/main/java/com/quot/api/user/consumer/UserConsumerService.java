package com.quot.api.user.consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quot.api.dto.BulkUserResponse;
import com.quot.api.dto.GetBulkUsersRequestWrapper;
import com.quot.api.dto.UserDTO;

@Service
public class UserConsumerService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserConsumerService.class);
	
	@Autowired
	private UserConsumerClient userClient;

	public UserDTO getUserFromUsername(String username, String token)
	{
		LOGGER.trace("in getUserFromUserName... requested username : " + username);
		UserDTO user = new UserDTO();
		ObjectMapper mapper = new ObjectMapper();
		try
		{
			ResponseEntity<Map<String, Object>> userResponse = userClient.getUserFromUsername(username, token);
			LOGGER.trace("userResponse from component user : " + userResponse);
			
			if(userResponse != null && userResponse.getStatusCode().equals(HttpStatus.OK))
			{
				user = mapper.convertValue(userResponse.getBody().get("user"), UserDTO.class);
				LOGGER.trace("user : " + user.toString());
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in userConsumerService : " + err.getMessage());
			user = null;
		}
		
		LOGGER.trace("user data from the client : " + user);
		return user;
	}
	
	/**
	 * 
	 * Consumer method to update profilePresent boolean for user associated to requested username
	 * 
	 * @author Akshay
	 * @param username
	 * @param token
	 */
	@Async
	public void updateProfileBooleanForUser(String username, String token)
	{
		LOGGER.trace("in user consumer updateProfileBooleanForUser method... username : " + username);
		
		try 
		{
			userClient.updateProfileBooleanForUser(username, token);
			LOGGER.trace("Successfully udpated profilePresent boolean for user associated to " + username);
		}
		catch (Exception err) 
		{
			LOGGER.error("Error in user consumer updateProfileBooleanForUser method : " + err);
		}
	}

	/**
	 * 
	 * Consumer method to get request followers/followings for a user
	 * 
	 * @author Akshay
	 * @param followers
	 * @return
	 */
	@SuppressWarnings({ "unchecked" })
	public List<BulkUserResponse> getBulkUsers(List<String> followers, String token)
	{
		LOGGER.trace("in user-consumer's getBulkUsers");
		List<BulkUserResponse> users = new ArrayList<>();
		ResponseEntity<Map<String, Object>> response = null;
		ObjectMapper mapper = new ObjectMapper();
		
		try 
		{
			GetBulkUsersRequestWrapper requestWrapper = new GetBulkUsersRequestWrapper();
			requestWrapper.setUsers(followers);
			response = userClient.getBulkUsers(requestWrapper, token);
			LOGGER.trace("response from component-user : " + response);
			if(response.getStatusCode().equals(HttpStatus.OK))
			{
				LOGGER.trace("Response status is 200 OK");
				users = mapper.convertValue(response.getBody().get("users"), ArrayList.class);
			}
			if(response.getStatusCode().equals(HttpStatus.NO_CONTENT))
			{
				LOGGER.trace("Response status is 204 No Content. Users not found for requested username");
				return users;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getBulkUsers : " + err);
		}
		
		LOGGER.trace("Response from getBulkUsers : " + users);
		return users;
	}

}
