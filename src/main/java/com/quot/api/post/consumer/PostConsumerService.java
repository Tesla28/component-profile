package com.quot.api.post.consumer;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.quot.api.post.dto.AddPostRequestWrapper;

@Service
public class PostConsumerService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(PostConsumerService.class);
	
	@Autowired
	private PostClient postClient;
	
	public boolean isPostPresent(String postId, String token)
	{
		LOGGER.trace("in isPostPresent consumer method... postId : " + postId);
		boolean isPostPresent = false;
		
		try
		{
			ResponseEntity<Map<String, Object>> response = postClient.getPostById(postId, token);
			if(response.getStatusCode().equals(HttpStatus.OK))
			{
				LOGGER.trace("Post associated to reuqested postId is present");;
				isPostPresent = true;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in isPostPresent : " + err);
			isPostPresent = false;
		}
		
		LOGGER.trace("Response from isPostPresent : " + isPostPresent);
		return isPostPresent;
	}

	/**
	 * 
	 * Post Component's consumer method to add user journal as a user post
	 * 
	 * @author Akshay
	 * @param addPostRequest
	 * @param token
	 * @return
	 */
	public Map<String, Object> addPostRequest(AddPostRequestWrapper addPostRequest, String token)
	{
		LOGGER.trace("in addPostRequest consumer method");
		Map<String, Object> postResponseMap = new HashMap<>();
		
		try
		{
			ResponseEntity<Map<String, Object>> response = postClient.addPost(addPostRequest, token);
			return response.getBody();
		}
		catch(Exception err)
		{
			LOGGER.error("Error while adding user journal as a post : " + err);
			postResponseMap.put("statusCode", 500);
			postResponseMap.put("statusMessage", "Server Error");
			postResponseMap.put("message", "Error while adding user journal as a post : " + err);
			return postResponseMap;
		}
	}
	
}
