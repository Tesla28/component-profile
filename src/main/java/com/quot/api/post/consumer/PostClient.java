package com.quot.api.post.consumer;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.quot.api.post.dto.AddPostRequestWrapper;

@FeignClient(name = "COMPONENT-POST", url = "localhost:7031")
public interface PostClient
{
	
	@RequestMapping(path = "/quot/api/post/getById", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Object>> getPostById(@RequestParam(name = "postId") String postId, @RequestHeader("Authorization") String token);

	@RequestMapping(path = "/quot/api/post/add", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> addPost(@RequestBody AddPostRequestWrapper addPostRequest, @RequestHeader("Authorization") String token);
}
