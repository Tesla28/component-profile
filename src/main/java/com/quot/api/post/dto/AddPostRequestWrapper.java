package com.quot.api.post.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "postTitle", "postText", "postType", "hashtags", "isPaged"})
public class AddPostRequestWrapper implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8154242249943835006L;

	@JsonProperty("postTitle")
	private String title;

	@JsonProperty("postText")
	private String text;

	@JsonProperty("postType")
	private String postType;

	@JsonProperty("isPaged")
	private boolean isPaged;

	@JsonProperty("hashtags")
	private List<String> hashtags = null;

	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("postTitle")
	public String getTitle()
	{
		return title;
	}

	@JsonProperty("postTitle")
	public void setTitle(String title)
	{
		this.title = title;
	}

	@JsonProperty("postText")
	public String getText()
	{
		return text;
	}

	@JsonProperty("postText")
	public void setText(String text)
	{
		this.text = text;
	}

	@JsonProperty("postType")
	public String getPostType()
	{
		return postType;
	}

	@JsonProperty("postType")
	public void setPostType(String postType)
	{
		this.postType = postType;
	}

	@JsonProperty("isPaged")
	public boolean isPaged()
	{
		return isPaged;
	}

	@JsonProperty("isPaged")
	public void setPaged(boolean isPaged)
	{
		this.isPaged = isPaged;
	}
	
	@JsonProperty("hashtags")
	public List<String> getHashtags()
	{
		return hashtags;
	}

	@JsonProperty("hashtags")
	public void setHashtags(List<String> hashtags)
	{
		this.hashtags = hashtags;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}
