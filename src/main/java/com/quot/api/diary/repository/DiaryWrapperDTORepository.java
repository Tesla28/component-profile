package com.quot.api.diary.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.quot.api.diary.dto.DiaryWrapperDTO;

@Repository
public interface DiaryWrapperDTORepository extends MongoRepository<DiaryWrapperDTO, Integer>
{

	List<DiaryWrapperDTO> findAllByProfileId(List<String> profileIds);

}
