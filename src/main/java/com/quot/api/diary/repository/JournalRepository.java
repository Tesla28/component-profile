package com.quot.api.diary.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.quot.api.diary.dto.JournalDTO;

@Repository
public interface JournalRepository extends MongoRepository<JournalDTO, Integer>
{
	JournalDTO findById(String id);
	
	@Query("{'_id' : ?0, 'profileId': ?1}")
	JournalDTO findByIdAndProfileId(String journalId, String id);

	@Query("{'journalDate' : ?0, 'profileId': ?1}")
	JournalDTO findByJournalDateAndProfile(Date journalDate, String id);

	@Query("{'year' : ?0, 'profileId': ?1}")
	List<JournalDTO> findByYearAndProfileId(int year, String id);

	@Query("{'month': ?0, 'year' : ?1, 'profileId': ?2}")
	List<JournalDTO> findByMonthYearAndProfileId(String month, int year, String id);
}
