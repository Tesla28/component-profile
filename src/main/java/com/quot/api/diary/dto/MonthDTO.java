package com.quot.api.diary.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "days", "journals" })
public class MonthDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4744620100128744255L;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("days")
	private Integer days;
	
	@JsonProperty("journals")
	private List<String> journals;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("days")
	public Integer getDays()
	{
		return days;
	}

	@JsonProperty("days")
	public void setDays(Integer days)
	{
		this.days = days;
	}

	@JsonProperty("journals")
	public List<String> getJournals()
	{
		return journals;
	}

	@JsonProperty("journals")
	public void setJournals(List<String> journals)
	{
		this.journals = journals;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}