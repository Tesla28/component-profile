package com.quot.api.diary.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "journalId", "hashtags" })
public class PostUserJournalRequestWrapper implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1520981551641533290L;
	
	@JsonProperty("journalId")
	private String journalId;
	
	@JsonProperty("hashtags")
	private List<String> hashtags = null;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("journalId")
	public String getJournalId()
	{
		return journalId;
	}

	@JsonProperty("journalId")
	public void setJournalId(String journalId)
	{
		this.journalId = journalId;
	}

	@JsonProperty("hashtags")
	public List<String> getHashtags()
	{
		return hashtags;
	}

	@JsonProperty("hashtags")
	public void setHashtags(List<String> hashtags)
	{
		this.hashtags = hashtags;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}