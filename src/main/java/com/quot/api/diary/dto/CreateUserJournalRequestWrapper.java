package com.quot.api.diary.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "journalTitle", "journalText", "journalDate" })
public class CreateUserJournalRequestWrapper implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8635518997542481510L;
	
	@JsonProperty("journalTitle")
	private String journalTitle;
	
	@JsonProperty("journalText")
	private String journalText;
	
	@JsonProperty("journalDate")
	private Date journalDate;
	//private String journalDate;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("journalTitle")
	public String getJournalTitle()
	{
		return journalTitle;
	}

	@JsonProperty("journalTitle")
	public void setJournalTitle(String journalTitle)
	{
		this.journalTitle = journalTitle;
	}

	@JsonProperty("journalText")
	public String getJournalText()
	{
		return journalText;
	}

	@JsonProperty("journalText")
	public void setJournalText(String journalText)
	{
		this.journalText = journalText;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}
	
	@JsonProperty("journalDate")
	public Date getJournalDate()
	{
		return journalDate;
	}

	@JsonProperty("journalDate")
	public void setJournalDate(Date journalDate)
	{
		this.journalDate = journalDate;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}