package com.quot.api.diary.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "journalId", "journalText" })
public class EditUserJournalRequestWrapper implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1641686379374362616L;

	@JsonProperty("journalId")
	private String journalId;
	
	@JsonProperty("journalText")
	private String journalText;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("journalId")
	public String getJournalId()
	{
		return journalId;
	}

	@JsonProperty("journalId")
	public void setJournalId(String journalId)
	{
		this.journalId = journalId;
	}

	@JsonProperty("journalText")
	public String getJournalText()
	{
		return journalText;
	}

	@JsonProperty("journalText")
	public void setJournalText(String journalText)
	{
		this.journalText = journalText;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}