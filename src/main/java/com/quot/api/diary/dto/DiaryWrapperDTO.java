package com.quot.api.diary.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "profileId", "isEnabled", "diaries" })
@Document(collection = "user-diary-wrappers")
public class DiaryWrapperDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4357809657076222983L;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("profileId")
	private String profileId;
	
	@JsonProperty("isEnabled")
	private Boolean isEnabled;
	
	@JsonProperty("diaries")
	private List<String> diaries = null;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("profileId")
	public String getProfileId()
	{
		return profileId;
	}

	@JsonProperty("profileId")
	public void setProfileId(String profileId)
	{
		this.profileId = profileId;
	}

	@JsonProperty("isEnabled")
	public Boolean getIsEnabled()
	{
		return isEnabled;
	}

	@JsonProperty("isEnabled")
	public void setIsEnabled(Boolean isEnabled)
	{
		this.isEnabled = isEnabled;
	}

	@JsonProperty("diaries")
	public List<String> getDiaries()
	{
		return diaries;
	}

	@JsonProperty("diaries")
	public void setDiaries(List<String> diaries)
	{
		this.diaries = diaries;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}