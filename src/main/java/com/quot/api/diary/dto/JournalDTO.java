package com.quot.api.diary.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "profileId", "journalTitle", "journalText", "noOfCharacters", "journalDate", "day", "month", "year", "isPosted", "date", "updatedDate" })
@Document(collection = "user-diary-journals")
public class JournalDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9187614170822667265L;
	
	@JsonProperty("id")
	private String id;
	 
	@JsonProperty("profileId")
	private String profileId;
	
	@JsonProperty("journalTitle")
	private String journalTitle;
	
	@JsonProperty("journalText")
	private String journalText;
	
	@JsonProperty("noOfCharacters")
	private Integer noOfCharacters;
	
	@JsonProperty("journalDate")
	private Date journalDate;
	
	@JsonProperty("day")
	private int day;
	
	@JsonProperty("month")
	private String month;
	
	@JsonProperty("year")
	private int year;
	
	@JsonProperty("isPosted")
	private boolean isPosted;
	
	@JsonProperty("date")
	private Date date;
	
	@JsonProperty("updatedDate")
	private Date updatedDate;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("journalTitle")
	public String getJournalTitle()
	{
		return journalTitle;
	}

	@JsonProperty("journalTitle")
	public void setJournalTitle(String journalTitle)
	{
		this.journalTitle = journalTitle;
	}
	
	@JsonProperty("journalText")
	public String getJournalText()
	{
		return journalText;
	}

	@JsonProperty("journalText")
	public void setJournalText(String journalText)
	{
		this.journalText = journalText;
	}

	@JsonProperty("noOfCharacters")
	public Integer getNoOfCharacters()
	{
		return noOfCharacters;
	}

	@JsonProperty("noOfCharacters")
	public void setNoOfCharacters(Integer noOfCharacters)
	{
		this.noOfCharacters = noOfCharacters;
	}
	
	@JsonProperty("journalDate")
	public Date getJournalDate()
	{
		return journalDate;
	}

	@JsonProperty("journalDate")
	public void setJournalDate(Date journalDate)
	{
		this.journalDate = journalDate;
	}

	@JsonProperty("profileId")
	public String getProfileId()
	{
		return profileId;
	}

	@JsonProperty("profileId")
	public void setProfileId(String profileId)
	{
		this.profileId = profileId;
	}

	@JsonProperty("day")
	public int getDay()
	{
		return day;
	}

	@JsonProperty("day")
	public void setDay(int day)
	{
		this.day = day;
	}

	@JsonProperty("month")
	public String getMonth()
	{
		return month;
	}

	@JsonProperty("month")
	public void setMonth(String month)
	{
		this.month = month;
	}

	@JsonProperty("year")
	public int getYear()
	{
		return year;
	}

	@JsonProperty("year")
	public void setYear(int year)
	{
		this.year = year;
	}
	
	@JsonProperty("isPosted")
	public boolean isPosted()
	{
		return isPosted;
	}

	@JsonProperty("isPosted")
	public void setPosted(boolean isPosted)
	{
		this.isPosted = isPosted;
	}

	@JsonProperty("date")
	public Date getDate()
	{
		return date;
	}

	@JsonProperty("date")
	public void setDate(Date date)
	{
		this.date = date;
	}
	
	@JsonProperty("updatedDate")
	public Date getUpdatedDate()
	{
		return updatedDate;
	}

	@JsonProperty("updatedDate")
	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}