package com.quot.api.diary.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "year", "diaryName", "isActive", "months" })
@Document(collection = "user-year-dairys")
public class YearDiaryDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1566993226568880492L;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("year")
	private Integer year;
	
	@JsonProperty("diaryName")
	private String diaryName;
	
	@JsonProperty("isActive")
	private Boolean isActive;
	
	@JsonProperty("months")
	private List<String> months = null;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("year")
	public Integer getYear()
	{
		return year;
	}

	@JsonProperty("year")
	public void setYear(Integer year)
	{
		this.year = year;
	}

	@JsonProperty("diaryName")
	public String getDiaryName()
	{
		return diaryName;
	}

	@JsonProperty("diaryName")
	public void setDiaryName(String diaryName)
	{
		this.diaryName = diaryName;
	}

	@JsonProperty("isActive")
	public Boolean getIsActive()
	{
		return isActive;
	}

	@JsonProperty("isActive")
	public void setIsActive(Boolean isActive)
	{
		this.isActive = isActive;
	}

	@JsonProperty("months")
	public List<String> getMonths()
	{
		return months;
	}

	@JsonProperty("months")
	public void setMonths(List<String> months)
	{
		this.months = months;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}