package com.quot.api.multi_threading.config;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.quot.api.constants.ProfileConstants;

@Configuration
@EnableAsync
public class CompletableFutureConfiguration
{
	@Bean(name = ProfileConstants.YEAR_DIARY_TASK_EXECUTOR_BEAN)
    public Executor taskExecutor(){
        ThreadPoolTaskExecutor executor=new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(100);
        executor.setQueueCapacity(100);
        executor.setThreadNamePrefix(ProfileConstants.YEAR_DIARY_THREAD_NAME_PREFIX);
        executor.initialize();
        return executor;
    }
}
