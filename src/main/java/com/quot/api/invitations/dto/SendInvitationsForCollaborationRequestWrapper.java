package com.quot.api.invitations.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "shelfId", "users" })
public class SendInvitationsForCollaborationRequestWrapper implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2748439175058505328L;
	
	@JsonProperty("shelfId")
	private String shelfId;
	
	@JsonProperty("users")
	private List<String> users = null;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("shelfId")
	public String getShelfId()
	{
		return shelfId;
	}

	@JsonProperty("shelfId")
	public void setShelfId(String shelfId)
	{
		this.shelfId = shelfId;
	}

	@JsonProperty("users")
	public List<String> getUsers()
	{
		return users;
	}

	@JsonProperty("users")
	public void setusers(List<String> users)
	{
		this.users = users;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}