package com.quot.api.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "username", "bio", "interests", "social", "location"})
public class ProfileRequestDTO
{
	
    @JsonProperty("bio")
	private String bio;
	
	@JsonProperty("interests")
	private List<String> interests = null;
	
	@JsonProperty("social")
	private Map<String, String> social;
	
	@JsonProperty("location")
	private Location location;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("bio")
	public String getBio()
	{
		return bio;
	}

	@JsonProperty("bio")
	public void setBio(String bio)
	{
		this.bio = bio;
	}

	@JsonProperty("interests")
	public List<String> getInterests()
	{
		return interests;
	}

	@JsonProperty("interests")
	public void setInterests(List<String> interests)
	{
		this.interests = interests;
	}

	@JsonProperty("social")
	public Map<String, String> getSocial()
	{
		return social;
	}

	@JsonProperty("social")
	public void setSocial(Map<String, String> social)
	{
		this.social = social;
	}

	@JsonProperty("location")
	public Location getLocation()
	{
		return location;
	}

	@JsonProperty("location")
	public void setLocation(Location location)
	{
		this.location = location;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}
