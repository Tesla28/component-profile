package com.quot.api.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "id", "username", "email", "avatar", "dob", "gender", "date", "updatedDate", "profilePresent" })
@Document(collection = "users")
public class UserResponseDTO
{
	@JsonProperty("id")
	private String id;

	@JsonProperty("username")
	private String username;

	@JsonProperty("email")
	private String email;

	@JsonProperty("avatar")
	private String avatar;

	@JsonProperty("dob")
	private Date dob;

	@JsonProperty("gender")
	private String gender;

	@JsonProperty("profilePresent")
	private boolean profilePresent;

	@JsonProperty("date")
	private Date date;

	@JsonProperty("updatedDate")
	private Date updateDate;

	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	public UserResponseDTO()
	{

	}

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("username")
	public String getUsername()
	{
		return username;
	}

	@JsonProperty("username")
	public void setUsername(String username)
	{
		this.username = username;
	}

	@JsonProperty("email")
	public String getEmail()
	{
		return email;
	}

	@JsonProperty("email")
	public void setEmail(String email)
	{
		this.email = email;
	}

	@JsonProperty("avatar")
	public String getAvatar()
	{
		return avatar;
	}

	@JsonProperty("avatar")
	public void setAvatar(String avatar)
	{
		this.avatar = avatar;
	}

	@JsonProperty("dob")
	public Date getDob()
	{
		return dob;
	}

	@JsonProperty("dob")
	public void setDob(Date dob)
	{
		this.dob = dob;
	}

	@JsonProperty("gender")
	public String getGender()
	{
		return gender;
	}

	@JsonProperty("gender")
	public void setGender(String gender)
	{
		this.gender = gender;
	}

	@JsonProperty("profilePresent")
	public boolean isProfilePresent()
	{
		return profilePresent;
	}

	@JsonProperty("profilePresent")
	public void setProfilePresent(boolean profilePresent)
	{
		this.profilePresent = profilePresent;
	}

	@JsonProperty("date")
	public Date getDate()
	{
		return date;
	}

	@JsonProperty("date")
	public void setDate(Date date)
	{
		this.date = date;
	}

	@JsonProperty("updatedDate")
	public Date getUpdateDate()
	{
		return updateDate;
	}

	@JsonProperty("updatedDate")
	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}