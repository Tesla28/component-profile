package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "shelfId", "shelfName", "isPrivate", "isCollaborative", "shelfType", "noOfPosts", "collaborators", "createdDate" })
public class ShelfPostResponseDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7961675685623475454L;

	@JsonProperty("shelfId")
	private String shelfId;
	
	@JsonProperty("shelfName")
	private String shelfName;
	
	@JsonProperty("isPrivate")
	private Boolean isPrivate;
	
	@JsonProperty("isCollaborative")
	private Boolean isCollaborative;
	
	@JsonProperty("shelfType")
	private String shelfType;
	
	@JsonProperty("noOfPosts")
	private Integer noOfPosts;
	
	@JsonProperty("collaborators")
	private List<String> collaborators;
	
	@JsonProperty("createdDate")
	private Date createdDate;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("shelfId")
	public String getShelfId()
	{
		return shelfId;
	}

	@JsonProperty("shelfId")
	public void setShelfId(String shelfId)
	{
		this.shelfId = shelfId;
	}

	@JsonProperty("shelfName")
	public String getShelfName()
	{
		return shelfName;
	}

	@JsonProperty("shelfName")
	public void setShelfName(String shelfName)
	{
		this.shelfName = shelfName;
	}

	@JsonProperty("isPrivate")
	public Boolean getIsPrivate()
	{
		return isPrivate;
	}

	@JsonProperty("isPrivate")
	public void setIsPrivate(Boolean isPrivate)
	{
		this.isPrivate = isPrivate;
	}

	@JsonProperty("isCollaborative")
	public Boolean getIsCollaborative()
	{
		return isCollaborative;
	}

	@JsonProperty("isCollaborative")
	public void setIsCollaborative(Boolean isCollaborative)
	{
		this.isCollaborative = isCollaborative;
	}

	@JsonProperty("shelfType")
	public String getShelfType()
	{
		return shelfType;
	}

	@JsonProperty("shelfType")
	public void setShelfType(String shelfType)
	{
		this.shelfType = shelfType;
	}

	@JsonProperty("noOfPosts")
	public Integer getNoOfPosts()
	{
		return noOfPosts;
	}

	@JsonProperty("noOfPosts")
	public void setNoOfPosts(Integer noOfPosts)
	{
		this.noOfPosts = noOfPosts;
	}

	@JsonProperty("createdDate")
	public Date getCreatedDate()
	{
		return createdDate;
	}
	
	@JsonProperty("collaborators")
	public List<String> getCollaborators()
	{
		return collaborators;
	}

	@JsonProperty("collaborators")
	public void setCollaborators(List<String> collaborators)
	{
		this.collaborators = collaborators;
	}

	@JsonProperty("createdDate")
	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}