package com.quot.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
{ "userId", "profileId", "username", "avatar" })
public class BulkUserResponse implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 283937555995334694L;

	@JsonProperty("userId")
	private String userId;

	@JsonProperty("profileId")
	private String profileId;

	@JsonProperty("username")
	private String username;

	@JsonProperty("avatar")
	private String avatar;

	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("userId")
	public String getUserId()
	{
		return userId;
	}

	@JsonProperty("userId")
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	@JsonProperty("profileId")
	public String getProfileId()
	{
		return profileId;
	}

	@JsonProperty("profileId")
	public void setProfileId(String profileId)
	{
		this.profileId = profileId;
	}

	@JsonProperty("username")
	public String getUsername()
	{
		return username;
	}

	@JsonProperty("username")
	public void setUsername(String username)
	{
		this.username = username;
	}

	@JsonProperty("avatar")
	public String getAvatar()
	{
		return avatar;
	}

	@JsonProperty("avatar")
	public void setAvatar(String avatar)
	{
		this.avatar = avatar;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}