package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "postId", "addedBy", "date" })
public class PostData implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4334051413634888792L;
	
	@JsonProperty("postId")
	private String postId;
	
	@JsonProperty("addedBy")
	private String addedBy;
	
	@JsonProperty("date")
	private Date date;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("postId")
	public String getPostId()
	{
		return postId;
	}

	@JsonProperty("postId")
	public void setPostId(String postId)
	{
		this.postId = postId;
	}

	@JsonProperty("addedBy")
	public String getAddedBy()
	{
		return addedBy;
	}

	@JsonProperty("addedBy")
	public void setAddedBy(String addedBy)
	{
		this.addedBy = addedBy;
	}

	@JsonProperty("date")
	public Date getDate()
	{
		return date;
	}

	@JsonProperty("date")
	public void setDate(Date date)
	{
		this.date = date;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}