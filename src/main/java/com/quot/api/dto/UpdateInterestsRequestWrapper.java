package com.quot.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "profileId", "interests" })
public class UpdateInterestsRequestWrapper implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5794621057108525732L;

	@JsonProperty("profileId")
	private String profileId;
	
	@JsonProperty("interests")
	private List<String> interests = null;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("profileId")
	public String getProfileId()
	{
		return profileId;
	}

	@JsonProperty("profileId")
	public void setProfileId(String profileId)
	{
		this.profileId = profileId;
	}

	@JsonProperty("interests")
	public List<String> getInterests()
	{
		return interests;
	}

	@JsonProperty("interests")
	public void setInterests(List<String> interests)
	{
		this.interests = interests;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}
