package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "shelfName", "shelfMaster", "isDefault", "isPrivate", "isCollaborative", "shelfType", "members", "posts",
		"date", "updatedDate" })
@Document(collection = "user-shelfs")
public class ShelfDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7549354416768473797L;
	
	@JsonProperty("id")
	private String id;
	
	@JsonProperty("shelfName")
	private String shelfName;
	
	@JsonProperty("shelfMaster")
	private String shelfMaster;
	
	@JsonProperty("isPrivate")
	private Boolean isPrivate;
	
	@JsonProperty("isCollaborative")
	private Boolean isCollaborative;
	
	@JsonProperty("shelfType")
	private String shelfType;
	
	@JsonProperty("members")
	private List<String> members = null;
	
	@JsonProperty("posts")
	private List<PostData> posts = null;
	
	@JsonProperty("date")
	private Date date;
	
	@JsonProperty("updatedDate")
	private Date updatedDate;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public String getId()
	{
		return id;
	}

	@JsonProperty("id")
	public void setId(String id)
	{
		this.id = id;
	}

	@JsonProperty("shelfName")
	public String getShelfName()
	{
		return shelfName;
	}

	@JsonProperty("shelfName")
	public void setShelfName(String shelfName)
	{
		this.shelfName = shelfName;
	}

	@JsonProperty("shelfMaster")
	public String getShelfMaster()
	{
		return shelfMaster;
	}

	@JsonProperty("shelfMaster")
	public void setShelfMaster(String shelfMaster)
	{
		this.shelfMaster = shelfMaster;
	}

	@JsonProperty("isPrivate")
	public Boolean getIsPrivate()
	{
		return isPrivate;
	}

	@JsonProperty("isPrivate")
	public void setIsPrivate(Boolean isPrivate)
	{
		this.isPrivate = isPrivate;
	}

	@JsonProperty("isCollaborative")
	public Boolean getIsCollaborative()
	{
		return isCollaborative;
	}

	@JsonProperty("isCollaborative")
	public void setIsCollaborative(Boolean isCollaborative)
	{
		this.isCollaborative = isCollaborative;
	}

	@JsonProperty("shelfType")
	public String getShelfType()
	{
		return shelfType;
	}

	@JsonProperty("shelfType")
	public void setShelfType(String shelfType)
	{
		this.shelfType = shelfType;
	}

	@JsonProperty("members")
	public List<String> getMembers()
	{
		return members;
	}

	@JsonProperty("members")
	public void setMembers(List<String> members)
	{
		this.members = members;
	}

	@JsonProperty("posts")
	public List<PostData> getPosts()
	{
		return posts;
	}

	@JsonProperty("posts")
	public void setPosts(List<PostData> posts)
	{
		this.posts = posts;
	}

	@JsonProperty("date")
	public Date getDate()
	{
		return date;
	}

	@JsonProperty("date")
	public void setDate(Date date)
	{
		this.date = date;
	}

	@JsonProperty("updatedDate")
	public Date getUpdatedDate()
	{
		return updatedDate;
	}

	@JsonProperty("updatedDate")
	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}