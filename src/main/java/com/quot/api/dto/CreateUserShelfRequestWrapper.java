package com.quot.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "shelfName", "isPrivate", "isCollaborative" })
public class CreateUserShelfRequestWrapper implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3175766137237018734L;
	
	@JsonProperty("shelfName")
	private String shelfName;
	
	@JsonProperty("isPrivate")
	private Boolean isPrivate;
	
	@JsonProperty("isCollaborative")
	private Boolean isCollaborative;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("sheldName")
	public String getShelfName()
	{
		return shelfName;
	}

	@JsonProperty("sheldName")
	public void setShelfName(String shelfName)
	{
		this.shelfName = shelfName;
	}

	@JsonProperty("isPrivate")
	public Boolean getIsPrivate()
	{
		return isPrivate;
	}

	@JsonProperty("isPrivate")
	public void setIsPrivate(Boolean isPrivate)
	{
		this.isPrivate = isPrivate;
	}

	@JsonProperty("isCollaborative")
	public Boolean getIsCollaborative()
	{
		return isCollaborative;
	}

	@JsonProperty("isCollaborative")
	public void setIsCollaborative(Boolean isCollaborative)
	{
		this.isCollaborative = isCollaborative;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}
