package com.quot.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "shelfId", "shelfMaster", "collaborationDate" })
public class CollaborativeShelfDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7167949323869715212L;

	@JsonProperty("shelfId")
	private String shelfId;
	
	@JsonProperty("shelfMaster")
	private String shelfMaster;
	
	@JsonProperty("collaborationDate")
	private Date collaborationDate;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("shelfId")
	public String getShelfId()
	{
		return shelfId;
	}

	@JsonProperty("shelfId")
	public void setShelfId(String shelfId)
	{
		this.shelfId = shelfId;
	}

	@JsonProperty("shelfMaster")
	public String getShelfMaster()
	{
		return shelfMaster;
	}

	@JsonProperty("shelfMaster")
	public void setShelfMaster(String shelfMaster)
	{
		this.shelfMaster = shelfMaster;
	}

	@JsonProperty("collaborationDate")
	public Date getCollaborationDate()
	{
		return collaborationDate;
	}

	@JsonProperty("collaborationDate")
	public void setCollaborationDate(Date collaborationDate)
	{
		this.collaborationDate = collaborationDate;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties()
	{
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value)
	{
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}

}