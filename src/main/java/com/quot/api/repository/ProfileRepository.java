package com.quot.api.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.quot.api.dto.ProfileDTO;

@Repository
public interface ProfileRepository extends MongoRepository<ProfileDTO, Integer>
{

	ProfileDTO findByUsername(String username);
	
	ProfileDTO findById(String profileId);

	void deleteById(String profileId);

	@Query("{'username' : {$regex : ?0, $options: 'i'} }")
	List<ProfileDTO> findAllBySearchString(String searchString);

	ProfileDTO findByUserId(String userid);
}
