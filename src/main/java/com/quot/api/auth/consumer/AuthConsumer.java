package com.quot.api.auth.consumer;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quot.api.dto.UserDTO;

@Service
public class AuthConsumer
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthConsumer.class);
	
	@Autowired
	private AuthClient authClient;
	
	/**
	 * 
	 * Component auth consumer service method to verify if the requested token string is valid
	 * 
	 * @author Akshay
	 * @param token
	 * @return boolean isTokenValid
	 * @throws Exception
	 */
	public boolean isTokenValid(String token) throws Exception
	{
		LOGGER.trace("in isValidToken method");
		boolean isTokenValid = false;
		@SuppressWarnings("rawtypes")
		ResponseEntity response = null;
		try 
		{
			response = authClient.validateToken(token);
			LOGGER.trace("response from component auth : " + response);
			if(response.getStatusCode().equals(HttpStatus.OK))
			{
				@SuppressWarnings("unchecked")
				Map<String, Object> responseMap = (Map<String, Object>) response.getBody();
				isTokenValid = (boolean) responseMap.get("isTokenValid");
			}
		}catch(Exception err) 
		{
			LOGGER.error("Error in isTokenValid : " + err.getMessage());
		}
		
		LOGGER.trace("Response of isTokenValid : " + isTokenValid);
		return isTokenValid;
	}
	
	/**
	 * 
	 * Component auth consumer service method to get username from token
	 * 
	 * @author Akshay
	 * @param token
	 * @return Username String
	 * @throws Exception
	 */
	public String getUsernameFromToken(String token) throws Exception
	{
		String userNameFromToken = null;
		ObjectMapper mapper = new ObjectMapper();
		UserDTO user = null;
		
		try
		{
			String authToken = "Bearer " + token;
			ResponseEntity<Map<String, Object>> response = authClient.getUserNameFromToken(token, authToken);
			LOGGER.trace("Response from component-auth : " + response);
			if(response!=null && response.getStatusCode().equals(HttpStatus.OK))
			{
				user = mapper.convertValue(response.getBody().get("user"), UserDTO.class);
				userNameFromToken = user.getUsername();
			}
		}
		catch (Exception err)
		{
			userNameFromToken = null;
		}

		return userNameFromToken;
	}

}
