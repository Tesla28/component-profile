package com.quot.api.scheduler.service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.quot.api.diary.dto.DiaryWrapperDTO;
import com.quot.api.diary.repository.DiaryWrapperDTORepository;
import com.quot.api.dto.ProfileDTO;
import com.quot.api.repository.ProfileRepository;

@Service
public class DiaryScheduler
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DiaryScheduler.class);
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private DiaryWrapperDTORepository diaryWrapperDTORepository;
	
	/**
	 * 
	 * Scheduled service method to create new YearDiary for each user's profile
	 * 
	 * @author Akshay
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@Scheduled(cron = "0 1 0 1 1 *")
	public void createAndSaveYearDiaries() throws InterruptedException, ExecutionException
	{
		LOGGER.trace("in Scheduled createAndSaveYearDiaries");
		// Get all the user profiles present across the platform
		CompletableFuture<List<DiaryWrapperDTO>> diaryWrappersFuture = getDiaryWrapperFuture();
		
		//CompletableFuture<List<String>> yearDiaries1 = CompletableFuture.supplyAsync(() -> addYearDiaryToWrapper());
		
	}
	
	private List<String> addYearDiaryToWrapper()
	{
		return null;
	}

	@Async
	private void addYearDiaryToEveryProfile(CompletableFuture<List<ProfileDTO>> userProfilesFuture) throws InterruptedException, ExecutionException
	{
		LOGGER.trace("in addYearDiaryToEveryProfile... current thread : " + Thread.currentThread());
		CompletableFuture<List<DiaryWrapperDTO>> getDiaryWrapperFuture = getDiaryWrapperFuture();
		LOGGER.trace("getDiaryWrapperFuture : " + getDiaryWrapperFuture);
		
	}

	
	/**
	 * 
	 * Service method to get diaryWrappers related to all the user profiles
	 * 
	 * @param profileIds
	 * @return
	 */
	public CompletableFuture<List<DiaryWrapperDTO>> getDiaryWrapperFuture()
	{
		LOGGER.trace("in getDiaryWrapperFuture... current thread : "+ Thread.currentThread());
		long startTime = System.currentTimeMillis();
		List<DiaryWrapperDTO> diaryWrappers = diaryWrapperDTORepository.findAll();
		LOGGER.trace("Retireved all the diaryWrapperDTOs related to each user profile");
		long endTime = System.currentTimeMillis();
		LOGGER.trace("Total time required for retrieving diaryWrappers related to each user profile : {}", (endTime - startTime));
		return CompletableFuture.completedFuture(diaryWrappers);
	}

	@Async
	public CompletableFuture<List<ProfileDTO>> getAllUserProfiles()
	{
		LOGGER.trace("in getAllUserProfiles");
		long startTime = System.currentTimeMillis();
		List<ProfileDTO> userProfiles = profileRepository.findAll();
		LOGGER.trace("Retireved all the user profiles");
		long endTime = System.currentTimeMillis();
		LOGGER.trace("Total time required for retrieving user profiles : {}", (endTime - startTime));
		return CompletableFuture.completedFuture(userProfiles);
	}
		
}
