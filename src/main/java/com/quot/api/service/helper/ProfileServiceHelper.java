package com.quot.api.service.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quot.api.constants.ProfileConstants;
import com.quot.api.dto.UserDTO;
import com.quot.api.user.consumer.UserConsumerService;
import com.quot.api.util.JwtUtil;

@Service
public class ProfileServiceHelper
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileServiceHelper.class);
	
	@Autowired
	private UserConsumerService userConsumerService;
	
	@Autowired
	private JwtUtil jwtUtil;

	/**
	 * Helper method to check if token is valid
	 * 
	 * @author Akshay
	 * @param token
	 * @param userName
	 * @return
	 */
	public boolean isTokenValid(String token, String username)
	{
		LOGGER.trace("in isTokenValid service helper method");
		boolean isTokenValid = false;
		UserDTO user = new UserDTO();
		try
		{
			user = userConsumerService.getUserFromUsername(username, token);
			LOGGER.trace("user from user component : " + user);
			
			isTokenValid = jwtUtil.validateToken(token, user);
		}
		catch (Exception err)
		{
			LOGGER.error("Error in isTokenValid service helper : " + err.getMessage());
			isTokenValid = false;
		}

		LOGGER.trace("isTokenValid : " + isTokenValid);		
		return isTokenValid;
	}
	
	/**
	 * Helper method to get the formatted date time from the input date
	 * 
	 * @author Akshay
	 * @param datetime
	 * @return
	 * @throws ParseException 
	 */
	public Date parseFormattedDateTime() throws ParseException
	{
		Date currentDate = new Date();
		LOGGER.trace("Current unformatter date : " + currentDate);
		String datePattern = "yyyy-MM-dd HH:mm:ss.SSS";
		SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
		String parsedDateString = sdf.format(currentDate);
		Date parsedDate = sdf.parse(parsedDateString);
		LOGGER.trace("current formatted date : " + parsedDate);
		return parsedDate;
	}

	
	/**
	 * 
	 * Helper method to get the formatted date from the input date
	 * 
	 * @author Akshay
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public Date parseFormattedDate() throws ParseException
	{
		Date currentDate = new Date();
		LOGGER.trace("Current unformatter date : " + currentDate);
		String datePattern = "yyyy-MM-dd";
		SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
		String parsedDateString = sdf.format(currentDate);
		Date parsedDate = sdf.parse(parsedDateString);
		LOGGER.trace("current formatted date : " + parsedDate);
		return parsedDate;
	}
	
	/**
	 * 
	 * Helper method to generate user shelfType based on 3 boolean inputs
	 * 
	 * 
	 * @author Akshay
	 * @param isDefault
	 * @param isPrivate
	 * @param isCollaborative
	 * @return String shelfType
	 */
	public String generateShelfType(Boolean isPrivate, Boolean isCollaborative)
	{
		LOGGER.trace(
				"in generateShelfType helper... isPrivate : " + isPrivate + ", isCollaborative : " + isCollaborative);
		StringBuilder shelfTypeBuilder = new StringBuilder();
		
		// initial part
		if(isPrivate)
		{
			shelfTypeBuilder.append(ProfileConstants.USER_SHELF_PRIVATE);
		}
		else
		{
			shelfTypeBuilder.append(ProfileConstants.USER_SHELF_PUBLIC);
		}
		
		shelfTypeBuilder.append("-");
		
		if(isCollaborative)
		{
			shelfTypeBuilder.append(ProfileConstants.USER_SHELF_COLLABORATIVE);
		}
		else
		{
			shelfTypeBuilder.append(ProfileConstants.USER_SHELF_NON_COLLABORATIVE);
		}
		
		LOGGER.trace("Response from generateShelfType : " + shelfTypeBuilder.toString());
		return shelfTypeBuilder.toString();
	}
	

	/**
	 * 
	 * Helper method to generate unique id for user shelf
	 * 
	 * @author Akshay
	 * @param shelfType
	 * @return String shelfId
	 */
	public String generateShelfId(String shelfType)
	{
		LOGGER.trace("in generateShelfId helper");
		String shelfId = null;
		StringBuilder shelfTypeInitialsBuilder = new StringBuilder();
		
		if(shelfType.contains(ProfileConstants.USER_SHELF_PRIVATE))
		{
			shelfTypeInitialsBuilder.append("pr");
		}
		else
		{
			shelfTypeInitialsBuilder.append("p");
		}
		
		if(shelfType.contains(ProfileConstants.USER_SHELF_NON_COLLABORATIVE))
		{
			shelfTypeInitialsBuilder.append("nc");
		}
		else
		{
			shelfTypeInitialsBuilder.append("c");
		}
		LOGGER.trace("shelf type initials : " + shelfTypeInitialsBuilder);
		
		UUID uuid = UUID.randomUUID();
		shelfId = ProfileConstants.USER_SHELF_INITIAL + "_" + shelfTypeInitialsBuilder.toString() + "_" + uuid.toString();
		
		LOGGER.trace("Response from generateShelfId : " + shelfId);
		return shelfId;
	}

	/**
	 * 
	 * Helper method to get year from requested date string
	 * 
	 * @author Akshay
	 * @param journalDate
	 * @return Integer year
	 */
	@SuppressWarnings("deprecation")
	public int getYearFromDate(Date journalDate)
	{
		LOGGER.trace("in getYearFromDate helper method... requestedDate : " + journalDate);
		return Integer.valueOf(journalDate.getYear() + 1900);
	}

	/**
	 * 
	 * Helper method to get month from requested date string
	 * 
	 * @author Akshay
	 * @param journalDate
	 * @return String month
	 */
	@SuppressWarnings("deprecation")
	public String getMonthFromDate(Date journalDate)
	{
		LOGGER.trace("in getMonthFromDate helper method... requestedDate : " + journalDate);
		String month = null;
		int monthFromDate = journalDate.getMonth();
		
		if(monthFromDate == 0)
		{
			month = "JAN";
		}
		else if(monthFromDate == 1)
		{
			month = "FEB";
		}
		else if(monthFromDate == 2)
		{
			month = "MAR";
		}
		else if(monthFromDate == 3)
		{
			month = "APR";
		}
		else if(monthFromDate == 4)
		{
			month = "MAY";
		}
		else if(monthFromDate == 5)
		{
			month = "JUN";
		}
		else if(monthFromDate == 6)
		{
			month = "JUL";
		}
		else if(monthFromDate == 7)
		{
			month = "AUG";
		}
		else if(monthFromDate == 8)
		{
			month = "SEP";
		}
		else if(monthFromDate == 9)
		{
			month = "OCT";
		}
		else if(monthFromDate == 10)
		{
			month = "NOV";
		}
		else if(monthFromDate == 11)
		{
			month = "DEC";
		}
		
		LOGGER.trace("Month response from requested date : " + month);
		return month;
	}

	/**
	 * 
	 * Helper method to get day of month from requested date string
	 * 
	 * @author Akshay
	 * @param journalDate
	 * @return String month
	 * @throws ParseException 
	 */
	@SuppressWarnings("deprecation")
	public int getDayFromDate(Date journalDate) throws ParseException
	{
		LOGGER.trace("in getDayFromDate helper method... requestedDate : " + journalDate);
// 		String dateString = journalDate.toString();
		
//		String datePattern = "yyyy-mm-dd";
//		SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
//		String parsedDateString = sdf.format(journalDate);
//		Date parsedDate = sdf.parse(parsedDateString);
//		LOGGER.trace("parsed date : " + parsedDate);
//		
//		LocalDate currentLocalDate = LocalDate.parse(parsedDate.toString());
//		LOGGER.trace("Current local date : " + currentLocalDate);
		
		return Integer.valueOf(journalDate.getDate());
	}

	
	/**
	 * 
	 * Helper method get no.of days difference between 2 dates
	 * 
	 * @author Akshay
	 * @param journalDate
	 * @param currentDate
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public int getDifferenceInDays(Date journalDate, Date currentDate)
	{
		LOGGER.trace("in getDifferenceInDays");
		int daysDifference = 0;

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		String inputString1 = String.valueOf(journalDate.getDate()) + "-" + String.valueOf(journalDate.getMonth()) + "-"
				+ String.valueOf(journalDate.getYear() + 1900);
		LOGGER.trace("input date string 1 : " + inputString1);
		String inputString2 = String.valueOf(currentDate.getDate()) + "-" + String.valueOf(currentDate.getMonth()) + "-"
				+ String.valueOf(currentDate.getYear() + 1900);
		LOGGER.trace("input date string 2 : " + inputString2);

		LocalDate date1 = LocalDate.parse(inputString1, dtf);
		LocalDate date2 = LocalDate.parse(inputString2, dtf);
		daysDifference = (int) Duration.between(date1, date2).toDays();
		
		LOGGER.trace("Response from getDifferenceInDays : " + daysDifference + " days");
		return daysDifference;
	}

	/**
	 * 
	 * Helper method to get exact number of month according to requested month name
	 * 
	 * @author Akshay
	 * @param month
	 * @return
	 */
	public int getMonthInteger(String month)
	{
		LOGGER.trace("in getMonthInteger helper method");
		int monthInteger = -1;
		
		if(month.equalsIgnoreCase("JAN"))
		{
			monthInteger = 0;
		}
		else if(month.equalsIgnoreCase("FEB"))
		{
			monthInteger = 1;
		}
		else if(month.equalsIgnoreCase("MAR"))
		{
			monthInteger = 2;
		}
		else if(month.equalsIgnoreCase("APR"))
		{
			monthInteger = 3;
		}
		else if(month.equalsIgnoreCase("MAY"))
		{
			monthInteger = 4;
		}
		else if(month.equalsIgnoreCase("JUN"))
		{
			monthInteger = 5;
		}
		else if(month.equalsIgnoreCase("JUL"))
		{
			monthInteger = 6;
		}
		else if(month.equalsIgnoreCase("AUG"))
		{
			monthInteger = 7;
		}
		else if(month.equalsIgnoreCase("SEP"))
		{
			monthInteger = 8;
		}
		else if(month.equalsIgnoreCase("OCT"))
		{
			monthInteger = 9;
		}
		else if(month.equalsIgnoreCase("NOV"))
		{
			monthInteger = 10;
		}
		else if(month.equalsIgnoreCase("DEC"))
		{
			monthInteger = 11;
		}
		else
		{
			monthInteger = -1;
		}
		
		LOGGER.trace("Response from getMonthInteger : " + monthInteger);
		return monthInteger;
	}

}
