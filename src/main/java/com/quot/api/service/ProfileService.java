package com.quot.api.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quot.api.auth.consumer.AuthConsumer;
import com.quot.api.constants.ProfileConstants;
import com.quot.api.diary.dto.CreateUserJournalRequestWrapper;
import com.quot.api.diary.dto.DiaryWrapperDTO;
import com.quot.api.diary.dto.EditUserJournalRequestWrapper;
import com.quot.api.diary.dto.JournalDTO;
import com.quot.api.diary.dto.PostUserJournalRequestWrapper;
import com.quot.api.diary.repository.DiaryWrapperDTORepository;
import com.quot.api.diary.repository.JournalRepository;
import com.quot.api.dto.BulkProfilesRequestDTO;
import com.quot.api.dto.BulkUserResponse;
import com.quot.api.dto.CollaborativeShelfDTO;
import com.quot.api.dto.CreateUserShelfRequestWrapper;
import com.quot.api.dto.Follower;
import com.quot.api.dto.Following;
import com.quot.api.dto.InvitationDTO;
import com.quot.api.dto.PostData;
import com.quot.api.dto.ProfileDTO;
import com.quot.api.dto.ProfileRequestDTO;
import com.quot.api.dto.ShelfDTO;
import com.quot.api.dto.ShelfPostResponseDTO;
import com.quot.api.dto.UpdateBioRequestWrapper;
import com.quot.api.dto.UpdateInterestsRequestWrapper;
import com.quot.api.dto.UserDTO;
import com.quot.api.invitations.dto.SendInvitationsForCollaborationRequestWrapper;
import com.quot.api.post.consumer.PostConsumerService;
import com.quot.api.post.dto.AddPostRequestWrapper;
import com.quot.api.repository.ProfileRepository;
import com.quot.api.service.helper.ProfileServiceHelper;
import com.quot.api.shelf.repository.ShelfRepository;
import com.quot.api.user.consumer.UserConsumerService;

@Service
public class ProfileService
{	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileService.class);
	
	@Autowired
	private UserConsumerService userConsumerService;
	
	@Autowired
	private ProfileRepository profileRepository;
	
	@Autowired
	private AuthConsumer authConsumer;
	
	@Autowired
	private ProfileServiceHelper profileServiceHelper;
	
	@Autowired 
	private ShelfRepository shelfRepository;
	
	@Autowired
	private PostConsumerService postConsumerService;
	
	@Autowired
	private DiaryWrapperDTORepository diaryWrapperDTORepository;
	
	@Autowired
	private JournalRepository journalRepository;
	
	@SuppressWarnings("deprecation")
	public Map<String, Object> createProfileService(ProfileRequestDTO profileRequestDto, String token)
	{
		LOGGER.trace("In createProfileService method");
		Map<String, Object> responseMap = new HashMap<>();
		ProfileDTO profile = new ProfileDTO();
		
		try
		{
			if (profileRequestDto.getInterests() == null || profileRequestDto.getInterests().size() < 3)
			{
				LOGGER.trace("Number of user interest selected are less than 3. Forbidden");
				responseMap.put("statusCode", 403);
				responseMap.put("statusMessage", "Forbidden");
				responseMap.put("message", "Number of user interest selected are less than 3. Forbidden");
				return responseMap;
			}	
			
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				// Check if logged in user is requesting to create profile
				String usernameFromToken = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("username from token : " + usernameFromToken);
				
				UserDTO user = userConsumerService.getUserFromUsername(usernameFromToken, token);
				if(user == null)
				{
					LOGGER.trace("User associated to username from request not found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "User associated to username from request not found");
					return responseMap;
				}

				// Check if profile already exits
				ProfileDTO tempProfile = profileRepository.findByUsername(usernameFromToken);
				if(tempProfile != null)
				{
					LOGGER.trace("User profile already present");
					responseMap.put("message", "Profile already present for the username " + usernameFromToken);
					responseMap.put("statusCode", 409);
					responseMap.put("statusMessage", "Conflict");
					return responseMap;
				}

				Date parsedFormatterDate = profileServiceHelper.parseFormattedDateTime();
				LOGGER.trace("parsed formatted current date : " + parsedFormatterDate);
				
				UUID uuid = UUID.randomUUID();
				profile.setId(ProfileConstants.VALUE_TYPE_PROFILE +  String.valueOf(uuid));
				profile.setBio(profileRequestDto.getBio());
				profile.setDate(parsedFormatterDate);
				profile.setUpdatedDate(parsedFormatterDate);
				profile.setUsername(user.getUsername());
				profile.setUserId(user.getId());
				profile.setCollaborationsAllowed(true);
				profile.setShelfs(new ArrayList<String>());
				profile.setFollowers(new ArrayList<Follower>());
				profile.setFollowings(new ArrayList<Following>());
				profile.setInvitations(new ArrayList<InvitationDTO>());
				profile.setCollaboratedShelfs(new ArrayList<CollaborativeShelfDTO>());
				profile.setSocial(profileRequestDto.getSocial());
				profile.setInterests(profileRequestDto.getInterests());
				profile.setLocation(profileRequestDto.getLocation());
				LOGGER.trace("created user profile : " + profile);
				profileRepository.save(profile);
				
				// Create a default private noncollab shelf
				CreateUserShelfRequestWrapper createUserShelfRequestWrapper = new CreateUserShelfRequestWrapper();
				createUserShelfRequestWrapper.setIsCollaborative(false);
				createUserShelfRequestWrapper.setIsPrivate(true);
				createUserShelfRequestWrapper.setShelfName("Bookmarks");
				Map<String, Object> shelfResponseMap = createUserShelfService(createUserShelfRequestWrapper, token);
				LOGGER.trace("user shelf creation service response : " + shelfResponseMap);
				if(!shelfResponseMap.get("statusCode").equals(new Integer(201)))
				{
					LOGGER.trace("Error in user bookmark shelf creation");
				}
				else
				{
					String generatedShelfId = (String) shelfResponseMap.get("shelfId");
					LOGGER.trace("Generated shelfId : " + generatedShelfId);
				}
				
				// Update isProfilePresent in user database
				userConsumerService.updateProfileBooleanForUser(usernameFromToken, token);
				LOGGER.trace("Updated profilePresent boolean for user associated to " + usernameFromToken);
				
				// Create user DiaryWrapper for the profile
//				creatDiaryWrapper(profile.getId());
//				LOGGER.trace("User diaryWrapper created");
				
				responseMap.put("profile", profile);
				responseMap.put("message", "User profile successfully created. Created");
				responseMap.put("statusMessage", "Created");
				responseMap.put("statusCode", 201);
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in createProfileService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
			responseMap.put("message", "Error in createProfileService : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from createProfile service : " + responseMap);
		return responseMap;
	}
	
	
	/**
	 * 
	 * Service method to create user profile diary wrapper
	 * 
	 * @author akshay
	 * @param id
	 */
	private void creatDiaryWrapper(String id)
	{
		LOGGER.trace("in creatDiaryWrapper");
		DiaryWrapperDTO userDiaryWrapper = new DiaryWrapperDTO();
		userDiaryWrapper.setId("diary-wrapper" + id.substring(7));
		LOGGER.trace("Generated id : {}", userDiaryWrapper.getId());
		userDiaryWrapper.setProfileId(id);
		userDiaryWrapper.setDiaries(new ArrayList<>());
		userDiaryWrapper.setIsEnabled(true);
		LOGGER.trace("User Diary wrapper : " + userDiaryWrapper.toString());
		diaryWrapperDTORepository.save(userDiaryWrapper);
		LOGGER.trace("saved user diary wrapper data to data table");
	}


	/**
	 * Service method to get a profile by input profileId
	 * 
	 * @author Akshay
	 * @param profileId
	 * @return
	 */
	public ProfileDTO getProfileById(String profileId)
	{
		LOGGER.trace("in getProfileById service method");
		ProfileDTO profile = null;
		try
		{
			LOGGER.trace("Authorizeds");
			profile = profileRepository.findById(profileId);
			LOGGER.trace("profile : " + profile);
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getProfileById service method : " + err.getMessage());
			profile = null;
		}

		return profile;
	}
	
	
	/**
	 * 
	 * Service method to get user's profile by userId
	 * 
	 * @author akshay
	 * @param userId
	 * @return
	 */
	public ProfileDTO getProfileByUserId(String userId)
	{
		LOGGER.trace("In getProfileByUserId... userId : " + userId);
		ProfileDTO profile = null;

		try
		{
			profile = profileRepository.findByUserId(userId);
			LOGGER.trace("profile : " + profile);
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getProfileByUserName service method : " + err.getMessage());
			profile = null;
		}

		LOGGER.trace("Response from getProfileByUserName : " + profile);
		return profile;
	}
	
	

	/**
	 * Service method to get a profile by input userName
	 * 
	 * @author Akshay
	 * @param userName
	 * @return
	 */
	public ProfileDTO getProfileByUserName(String userName)
	{
		LOGGER.trace("in getProfileByUserName service method");
		ProfileDTO profile = null;

		try
		{
			profile = profileRepository.findByUsername(userName);
			LOGGER.trace("profile : " + profile);
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getProfileByUserName service method : " + err.getMessage());
			profile = null;
		}

		LOGGER.trace("Response from getProfileByUserName : " + profile);
		return profile;
	}
	
	
	/**
	 * 
	 * Service method to get user profile by user token
	 * 
	 * @author Akshay
	 * @param token
	 * @return ProfileDTO
	 */
	public ProfileDTO getProfileByUserTokenService(String token)
	{
		LOGGER.trace("in getProfileByUserToken");
		ProfileDTO profile = null;
		String username = null;
		
		try
		{
			username = authConsumer.getUsernameFromToken(token.substring(7));
			LOGGER.trace("username from token : " + username);
			
			profile = getProfileByUserName(username);
			if(profile == null)
			{
				LOGGER.trace("profile associated to user token not found");
				profile = null;
			}
			else
			{
				LOGGER.trace("profile : " + profile.toString());
			}
			
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getProfileByUserTokenService : " + err);
			profile = null;
		}
		
		LOGGER.trace("Response from getProfileByUserTokenService : " + profile);
		return profile;
	}
	
	

	/**
	 * Service method to update a user profile
	 * @author Akshay
	 * @param requestProfileDto
	 * @return
	 */
	public Map<String, Object> udpdateProfile(ProfileDTO requestProfileDto, String token)
	{
		LOGGER.trace("in updateProfile service method");
		Map<String, Object> responseMap = new HashMap<>();
		String profileId = requestProfileDto.getId();
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{

				LOGGER.trace("Authorized");
				if(!isAllowedToEdit(token, requestProfileDto.getUsername()))
				{
					LOGGER.trace("User is not allowed the update/edit requested profile. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("message", "User is not allowed the update/edit requested profile. Forbidden");
					return responseMap;
				}

				// ProfileDTO presentProfileDTO = profileRepository.findById(profileId);
				ProfileDTO presentProfileDTO = this.getProfileById(profileId);
				LOGGER.trace("presentProfileDTO : " + presentProfileDTO);
				if(presentProfileDTO == null)
				{
					LOGGER.trace("Profile not found");
					String tempUsername = requestProfileDto.getUsername();
					responseMap.put("message", "No profile present for the username " + tempUsername);
					responseMap.put("statusCode", 204); // 204 for No Content
					return responseMap;
				}

				requestProfileDto.setUpdatedDate(profileServiceHelper.parseFormattedDateTime());
				LOGGER.trace("updated profile : " + requestProfileDto);
				profileRepository.save(requestProfileDto);

				presentProfileDTO = null;
				responseMap.put("message", "Profile updated successfully");
				responseMap.put("statusCode", 200);
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("message", "Unauthorized");
				return responseMap;
			}

		}
		catch (Exception err)
		{
			LOGGER.error("Error in updateProfile service : " + err.getMessage());
			responseMap.put("message", "Server error");
			responseMap.put("statusCode", 500); // 204 for Server Error
			return responseMap;
		}

		LOGGER.trace("Response from updateProfile service : " + responseMap);
		return responseMap;
	}

	/**
	 * Service method follow multiple user profiles and reflect input requested user to their respective profile's followers 
	 * 
	 * @author Akshay
	 * @param profileFollowRequestDTO
	 * @return
	 */
	public Map<String, Object> followService(String profileId, String token)
	{
		LOGGER.trace("in follow profile service");
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				// Get current logged in user's profile
				String currentLoggedInUsername = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Current logged in user : " + currentLoggedInUsername);

				ProfileDTO currentLoggedInUserProfile = profileRepository.findByUsername(currentLoggedInUsername);
				if (currentLoggedInUserProfile == null)
				{
					LOGGER.trace("No profile found for logged in user. No Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "No profile found for logged in user. No Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("current logged in user's profile : " + currentLoggedInUserProfile.toString());
				}
				
				if(currentLoggedInUserProfile.getId().equals(profileId))
				{
					LOGGER.trace("Self follow not allowed. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Self follow not allowed. Forbidden");
					return responseMap;
				}

				// Get the user's profile current user wants to follow
				ProfileDTO toBeFollowedProfile = profileRepository.findById(profileId);
				if (toBeFollowedProfile == null)
				{
					LOGGER.trace("No profile present for the requested profileId. Not Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "No profile present for the requested profileId. No Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("to be followed profile data : " + toBeFollowedProfile.toString());
				}

				Date currentDate = profileServiceHelper.parseFormattedDateTime();
				LOGGER.trace("Current date : " + currentDate.toString());
				if (toBeFollowedProfile.getFollowers().size() == 0)
				{
					LOGGER.trace("No followers currently present for the requested user profile to be followed");
					Follower currentFollower = new Follower();
					currentFollower.setProfileId(currentLoggedInUserProfile.getId());
					currentFollower.setUserId(currentLoggedInUserProfile.getUserId());
					currentFollower.setDate(currentDate);
					toBeFollowedProfile.getFollowers().add(currentFollower);
					LOGGER.trace("Following's profile : " + toBeFollowedProfile.toString());

					Following userFollowing = new Following();
					userFollowing.setProfileId(profileId);
					userFollowing.setUserId(toBeFollowedProfile.getUserId());
					userFollowing.setDate(currentDate);
					currentLoggedInUserProfile.getFollowings().add(userFollowing);
					LOGGER.trace("Follower's profile: " + userFollowing.toString());

					profileRepository.save(toBeFollowedProfile);
					profileRepository.save(currentLoggedInUserProfile);
					LOGGER.trace("Both profiles updated and saved");
				}
				else
				{
					LOGGER.trace("Iterrating all followings");
					Optional<Follower> possibleFollow = toBeFollowedProfile.getFollowers().stream()
							.filter(e -> e.getProfileId().equals(currentLoggedInUserProfile.getId())).findFirst();
					LOGGER.trace("possibleFollow is present : " + possibleFollow.isPresent());
					if (possibleFollow.isPresent())
					{
						LOGGER.trace("User already follows requested user's profile. Forbidden");
						responseMap.put("statusCode", 403);
						responseMap.put("statusMessage", "Forbidden");
						responseMap.put("message", "User already follows requested user's profile. Forbidden");
						return responseMap;
					}
					else
					{
						LOGGER.trace("User does not follow requested user's profile yet.");
						Follower currentFollower = new Follower();
						currentFollower.setProfileId(currentLoggedInUserProfile.getId());
						currentFollower.setUserId(currentLoggedInUserProfile.getUserId());
						currentFollower.setDate(currentDate);
						toBeFollowedProfile.getFollowers().add(currentFollower);
						LOGGER.trace("Following's profile : " + toBeFollowedProfile.toString());

						Following userFollowing = new Following();
						userFollowing.setProfileId(profileId);
						userFollowing.setUserId(toBeFollowedProfile.getUserId());
						userFollowing.setDate(currentDate);
						currentLoggedInUserProfile.getFollowings().add(userFollowing);
						LOGGER.trace("Follower's profile: " + userFollowing.toString());

						profileRepository.save(toBeFollowedProfile);
						profileRepository.save(currentLoggedInUserProfile);
						LOGGER.trace("Both profiles updated and saved");
					}
				}

				LOGGER.trace("Requested user's profile successfully followed by current user. Successful");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message",
						"Requested user's profile successfully followed by current user. Successful");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized for requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in follow service : " + err);
			responseMap.put("message", "Error in follow service : " + err);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("statusCode", 500);
			return responseMap;
		}

		LOGGER.trace("Response from followService : " + responseMap);
		return responseMap;
	}
	

	/**
	 * 
	 * Service method to unfollow user profile
	 * 
	 * @author Akshay
	 * @param profileFollowRequestDTO
	 * @return Map<String, Object>
	 */
	public Map<String, Object> unfollowService(String followingProfileId, String token)
	{
		LOGGER.trace("in follow profile service");
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("current user's username : " + username);

				ProfileDTO currentUserProfile = profileRepository.findByUsername(username);
				if (currentUserProfile == null)
				{
					LOGGER.trace("Current user does not have a profile associated. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Current user does not have a profile associated. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("current user profile : " + currentUserProfile.toString());
				}

				ProfileDTO followingUserProfile = profileRepository.findById(followingProfileId);
				if (followingUserProfile == null)
				{
					LOGGER.trace("Following User does not have a profile associated. Conflict");
					responseMap.put("statusCode", 409);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "Following User does not have a profile associated. Conflict");
					return responseMap;
				}
				else
				{
					LOGGER.trace("following User Profile : " + followingUserProfile.toString());
				}

				List<Follower> userFollowers = followingUserProfile.getFollowers();
				LOGGER.trace("User followers : " + userFollowers);
				List<Following> currentFollowings = currentUserProfile.getFollowings();
				LOGGER.trace("Current user followings : " + currentFollowings);
				
				if(userFollowers.size()==0)
				{
					LOGGER.trace("No followers present for the requested profile. Not Found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "No followers present for the requested profile. Not Found");
					return responseMap;
				}
				Optional<Follower> possibleFollower = userFollowers.stream()
						.filter(e -> e.getProfileId().equals(currentUserProfile.getId())).findFirst();
				LOGGER.trace("Optinal follower data : " + possibleFollower);
				if (!possibleFollower.isPresent())
				{
					LOGGER.trace("Current user is not following requested user profile. Conflict");
					responseMap.put("statusCode", 409);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "Current user is not following requested user profile. Conflict");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Current user is following requested user profile");
					userFollowers.remove(possibleFollower.get());
					LOGGER.trace("updated followers list for requested following user : " + userFollowers);
					profileRepository.save(followingUserProfile);
					LOGGER.trace("Added current user's username the requested users followers list");

					Optional<Following> possibleFollowing = currentFollowings.stream()
							.filter(e -> e.getProfileId().equals(followingUserProfile.getId())).findFirst();
					currentUserProfile.getFollowings().remove(possibleFollowing.get());
					profileRepository.save(currentUserProfile);

					responseMap.put("message", "Successfully unfollowed user");
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					LOGGER.trace("Success response from follow service : " + responseMap);
				}
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("message", "User is not authorized to perform requested action");
				responseMap.put("statusMessage", "Unauthorized");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in follow service : " + err);
			responseMap.put("message", "Error in unfollowService : " + err);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("statusCode", 500);
			return responseMap;
		}

		LOGGER.trace("Success response from follow service : " + responseMap);
		return responseMap;
	}
	
	
	/**
	 * 
	 * Service method to check if the current user is allowed to modify profile
	 * 
	 * @author Akshay
	 * @param token
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	public boolean isAllowedToEdit(String token, String userName) throws Exception
	{
		LOGGER.trace("in isAllowedToEdit service method");
		boolean isAllowedToEdit = false;
		
		String userNameFromToken = authConsumer.getUsernameFromToken(token.substring(7));
		LOGGER.trace("userNameFromToken : " + userNameFromToken);
		
		if(userNameFromToken != null && userNameFromToken!="")
		{
			isAllowedToEdit = userNameFromToken.equals(userName);
		}
		LOGGER.trace("response isAllowedToEdit : " + isAllowedToEdit);
		return isAllowedToEdit;
	}
	

	/**
	 * 
	 * Service method to get multiple profile data in bulk
	 * 
	 * @author Akshay
	 * @param bulkProfilesRequestDto
	 * @param token
	 * @return
	 */
	public Map<String, Object> getBulkProfilesDataService(BulkProfilesRequestDTO bulkProfilesRequestDto)
	{
		LOGGER.trace("In getBulkProfilesDataService");
		Map<String, Object> responseMap = new HashMap<>();
		List<ProfileDTO> profilesList = new ArrayList<>();
		try
		{
			if(bulkProfilesRequestDto.getProfiles() == null || bulkProfilesRequestDto.getProfiles().size() == 0)
			{
				LOGGER.trace("There are no profiles requested. Hence getting all profiles");
				profilesList.addAll(profileRepository.findAll());
				if(profilesList == null || profilesList.size() == 0)
				{
					LOGGER.trace("No profiles present");
					responseMap.put("statusCode", "204");
					responseMap.put("message", "No profiles present");
					return responseMap;
				}

				LOGGER.trace("profiles data : " + profilesList);
				responseMap.put("statusCode", "200");
				responseMap.put("message", "Successful");
				responseMap.put("data", profilesList);

			}
			else
			{
				LOGGER.trace("Requested profiles - " + bulkProfilesRequestDto.getProfiles());
				for (String eachProfile : bulkProfilesRequestDto.getProfiles())
				{
					ProfileDTO tempProfile = profileRepository.findById(eachProfile);
					if(tempProfile!= null)
					{
						profilesList.add(tempProfile);
						tempProfile = null;
					}
				}
				if(profilesList.size()==0)
				{
					LOGGER.trace("No profile data found");
					responseMap.put("statusCode", "204");
					responseMap.put("message", "No content");
					return responseMap;
				}
				
				LOGGER.trace("profiles data : " + profilesList);
				responseMap.put("statusCode", "200");
				responseMap.put("message", "Successful");
				responseMap.put("data", profilesList);
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in getBulkProfilesDataService : " + err.getMessage());
			responseMap.put("statusCode", "500");
			responseMap.put("message", "Server error");
		}

		LOGGER.trace("Response from getBulkProfilesDataService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to get following users of requested user
	 * 
	 * @author Akshay
	 * @param username
	 * @param token
	 * @return Map<String, Object> containing profiles information
	 */
	public Map<String, Object> getFollowingService(String profileId, String token)
	{
		LOGGER.trace("in getFollowingService... username : " + profileId);
		Map<String, Object> responseMap = new HashMap<>();
		
		try 
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				ProfileDTO currentLoggedInUserProfile = profileRepository.findById(profileId);
				if(currentLoggedInUserProfile == null)
				{
					LOGGER.trace("No profile found for logged in user. No Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "No profile found for logged in user. No Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("current logged in user's profile : " + currentLoggedInUserProfile.toString());
				}
				
				List<String> userIds = new ArrayList<>();
				List<Following> followingsList = currentLoggedInUserProfile.getFollowings();
				for(Following eachFollowingUser : followingsList)
				{
					LOGGER.trace("Current followed user : " + eachFollowingUser);
					userIds.add(eachFollowingUser.getUserId());
				}
				
				if (userIds.size() == 0)
				{
					LOGGER.trace("No followings present for the user");
					responseMap.put("users", new ArrayList<>());
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "User not following any other user's profile. No Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("userId list : " + userIds);
					List<BulkUserResponse> bulkUserResponseList = userConsumerService.getBulkUsers(userIds, token);
					LOGGER.trace("Users response data : " + bulkUserResponseList);

					if (bulkUserResponseList.size() == 0)
					{
						LOGGER.trace("User not following any other user's profile. No Content");
						responseMap.put("users", new ArrayList<>());
						responseMap.put("statusCode", 204);
						responseMap.put("statusMessage", "No Content");
						responseMap.put("message", "User not following any other user's profile. No Content");
						return responseMap;
					}

					responseMap.put("users", bulkUserResponseList);
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
				}
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized for requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getFollowings : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getFollowings : " + err);
		}
		
		LOGGER.trace("Response from getFollowingService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to get all the followers of the requested user
	 * 
	 * @author Akshay
	 * @param username
	 * @return Map<String, Object> containing followers profile data
	 */
	public Map<String, Object> getFollowersService(String profileId, String token)
	{
		LOGGER.trace("in getFollowersService... profileId : " + profileId);
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				ProfileDTO currentLoggedInUserProfile = profileRepository.findById(profileId);
				if (currentLoggedInUserProfile == null)
				{
					LOGGER.trace("No profile found for logged in user. No Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "No profile found for logged in user. No Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("current logged in user's profile : " + currentLoggedInUserProfile.toString());
				}

				List<String> userIds = new ArrayList<>();
				List<Follower> followersList = currentLoggedInUserProfile.getFollowers();
				for (Follower eachFollowerUser : followersList)
				{
					LOGGER.trace("Current followed user : " + eachFollowerUser);
					userIds.add(eachFollowerUser.getUserId());
				}
				
				if (userIds.size() == 0)
				{
					LOGGER.trace("No followings present for the user");
					responseMap.put("users", new ArrayList<>());
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "User not following any other user's profile. No Content");
					return responseMap;
				}
				
				LOGGER.trace("userId list : " + userIds);
				List<BulkUserResponse> bulkUserResponseList = userConsumerService.getBulkUsers(userIds, token);
				LOGGER.trace("Users response data : " + bulkUserResponseList);

				if (bulkUserResponseList.size() == 0)
				{
					LOGGER.trace("User has no followers. No Content");
					responseMap.put("users", new ArrayList<>());
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "User has no followers. No Content");
					return responseMap;
				}

				responseMap.put("users", bulkUserResponseList);
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");

			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}

		}
		catch (Exception err)
		{
			LOGGER.error("Error in getFollowersService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("stausMessage", "Server Error");
			responseMap.put("message", "Error in getFollowersService : " + err);
		}

		LOGGER.trace("Response from getFollowingService : " + responseMap);
		return responseMap;
	}

	
	
	/**
	 * 
	 * Service method to find a user profile
	 * 
	 * 
	 * @param username
	 * @param token
	 * @return
	 */
	public Map<String, Object> searchUserProfileService(String username, String searchString, String token)
	{
		LOGGER.trace("in searchUserProfileService");
		Map<String, Object> responseMap = new HashMap<>();
		UserDTO currentUser = null;
		List<ProfileDTO> userProfileList = new ArrayList<>(); 
		
		try 
		{
			if(authConsumer.isTokenValid(token.substring(7))) 
			{
				LOGGER.trace("User is authenticated");
				currentUser = userConsumerService.getUserFromUsername(username, token);
				if(currentUser == null) 
				{
					LOGGER.trace("No user present with username");
					responseMap.put("message", "No user present with username");
					responseMap.put("statusCode", 400);
					return responseMap;
				}
				
				userProfileList = profileRepository.findAllBySearchString(searchString);
				if(userProfileList != null && userProfileList.size()>0)
				{
					LOGGER.trace("user profiles found");
					responseMap.put("message", "Successful");
					responseMap.put("statusCode", 200);
					responseMap.put("userProfileList", userProfileList);
				}
				
			}
			else
			{
				LOGGER.trace("The user is not authorized");
				responseMap.put("statusCode", 401);
				responseMap.put("message", "Unauthorized");
				return responseMap;
			}
		}
		catch(Exception err) 
		{
			LOGGER.error("Error in searchUserProfileService : " + err.getMessage());
		}
		
		LOGGER.trace("Response from searchUserProfileService : " + responseMap);
		return responseMap;
	}

	/**
	 * 
	 * Service method to udpdate username from user profile on username update from component-user
	 * 
	 * @author Akshay
	 * @param currentUsername
	 * @param newUsername
	 * @param token
	 * @return
	 */
	public Map<String, Object> updateProfileUsername(String currentUsername, String newUsername, String token)
	{
		LOGGER.trace("in updateProfileUsername");
		Map<String, Object> responseMap = new HashMap<>();
		ProfileDTO currentUserProfile = null;
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				currentUserProfile = profileRepository.findByUsername(currentUsername);
				LOGGER.trace("Current user profile : " + currentUsername);
				if(currentUserProfile == null)
				{
					LOGGER.trace("Current user profile not present");
					responseMap.put("statusCode", 404);
					responseMap.put("message", "Current user profile not found");
					return responseMap;
				}
				
				currentUserProfile.setUsername(newUsername);
				currentUserProfile.setUpdatedDate(profileServiceHelper.parseFormattedDateTime());
				LOGGER.trace("Updated profile information : " + currentUserProfile);
				profileRepository.save(currentUserProfile);
				LOGGER.trace("User profile username updated");
				
				responseMap.put("statusCode", 200);
				responseMap.put("message", "Username update in user profile");
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("message", "Unauthorized");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in updateProfileUsername : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("message", "Server error");
		}
		
		LOGGER.trace("Response from updateProfileUsername : " + responseMap);
		return responseMap;
	}

	
	/**
	 * 
	 * Service method to update
	 * 
	 * 
	 * @param updateBioRequestWrapper
	 * @param token
	 * @return
	 */
	public Map<String, Object> updateProfileBioService(UpdateBioRequestWrapper updateBioRequestWrapper, String token)
	{
		LOGGER.trace("in updateProfileBioService... updateBioRequestWrapper : " + updateBioRequestWrapper);
		Map<String, Object> responseMap = new HashMap<>();
		
		try 
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				String usernameFromToken = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("username from token : " + usernameFromToken);
				
				ProfileDTO currentProfile = profileRepository.findById(updateBioRequestWrapper.getProfileId());
				if(currentProfile == null)
				{
					LOGGER.trace("User profile associated to requested profileId not found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "User profile associated to requested profileId not found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("current user profile : " + currentProfile.toString());
				}
				
				if(!currentProfile.getUsername().equals(usernameFromToken))
				{
					LOGGER.trace("Username in profile does not matches logged in user's username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Username in profile does not matches logged in user's username. Forbidden");
					return responseMap;
				}
				
				currentProfile.setBio(updateBioRequestWrapper.getBio());
				currentProfile.setUpdatedDate(profileServiceHelper.parseFormattedDateTime());
				LOGGER.trace("Updated profile data : " + currentProfile.toString());
				profileRepository.save(currentProfile);
				LOGGER.trace("Saved updated bio in user profile's data");
			
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "User profile's bio updated successfully");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.trace("Error in updateProfileBioService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in updateProfileBioService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from updateProfileBioService... responseMap : " + responseMap);
		return responseMap;
	}

	
	/**
	 * 
	 * Service method to update the interest from user profile
	 * 
	 * @author Akshay
	 * @param updateInterestsRequestWrapper
	 * @param token
	 * @return
	 */
	public Map<String, Object> updateProfileInterestsService(UpdateInterestsRequestWrapper updateInterestsRequestWrapper, String token)
	{
		LOGGER.trace("in updateProfileInterestsService... updateInterestsRequestWrapper : " + updateInterestsRequestWrapper);
		Map<String, Object> responseMap = new HashMap<>();
		
		try 
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				if (updateInterestsRequestWrapper.getInterests() == null
						|| updateInterestsRequestWrapper.getInterests().size() < 3)
				{
					LOGGER.trace("Minimun number of interests less than 3. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Minimun number of interests less than 3. Forbidden");
					return responseMap;
				}
				
				String usernameFromToken = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("username from token : " + usernameFromToken);
				
				ProfileDTO currentProfile = profileRepository.findById(updateInterestsRequestWrapper.getProfileId());
				if(currentProfile == null)
				{
					LOGGER.trace("User profile associated to requested profileId not found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "User profile associated to requested profileId not found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("current user profile : " + currentProfile.toString());
				}
				
				if(!currentProfile.getUsername().equals(usernameFromToken))
				{
					LOGGER.trace("Username in profile does not matches logged in user's username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Username in profile does not matches logged in user's username. Forbidden");
					return responseMap;
				}
				
				currentProfile.setInterests(updateInterestsRequestWrapper.getInterests());
				currentProfile.setUpdatedDate(profileServiceHelper.parseFormattedDateTime());
				LOGGER.trace("Updated profile data : " + currentProfile.toString());
				profileRepository.save(currentProfile);
				LOGGER.trace("Saved updated interests in user profile's data");
			
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "User profile's interests updated successfully");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in updateProfileInterestsService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in updateProfileInterestsService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from updateProfileInterestsService... responseMap : " + responseMap);
		return responseMap;
	}

	
	// ************************* User Shelf service methods ************************* //

	/**
	 * 
	 * Service method to create user shelf
	 * 
	 * @author Akshay
	 * @param createUserShelfRequestWrapper
	 * @param token
	 * @return
	 */
	public Map<String, Object> createUserShelfService(CreateUserShelfRequestWrapper createUserShelfRequestWrapper, String token)
	{
		LOGGER.trace("in createUserShelfService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Unauthorized");
				
				// Get user details from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);
				
				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if(loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("logged in user : " + loggedInUser.toString());
				}
				
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No profile is present for the logged-in user. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				
				// Generate a shelfType from 3 boolean fields from request
				String shelfType = profileServiceHelper.generateShelfType(createUserShelfRequestWrapper.getIsPrivate(),
						createUserShelfRequestWrapper.getIsCollaborative());
				LOGGER.trace("Shelf type : " + shelfType);
				
				// Generate unique id for shelf
				String id = profileServiceHelper.generateShelfId(shelfType);
				LOGGER.trace("shelf id : " + id);
			
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged-in user profile : " + loggedInUserProfile.toString());
				loggedInUserProfile.getShelfs().add(id);
				profileRepository.save(loggedInUserProfile);
				
				Date currentDate = profileServiceHelper.parseFormattedDateTime();
				LOGGER.trace("Current date : " + currentDate.toString());
				
				ShelfDTO shelf = new ShelfDTO();
				shelf.setId(id);
				shelf.setShelfName(createUserShelfRequestWrapper.getShelfName());
				shelf.setShelfMaster(loggedInUserProfile.getId());
				shelf.setIsPrivate(createUserShelfRequestWrapper.getIsPrivate());
				shelf.setIsCollaborative(createUserShelfRequestWrapper.getIsCollaborative());
				shelf.setShelfType(shelfType);
				shelf.setPosts(new ArrayList<PostData>());
				shelf.setMembers(new ArrayList<>());
				shelf.setDate(currentDate);
				shelf.setUpdatedDate(currentDate);
				
				shelfRepository.save(shelf);
				LOGGER.trace("Shelf data object created and saved : " + shelf.toString());
				
				responseMap.put("statusCode", 201);
				responseMap.put("statusMessage", "Created");
				responseMap.put("shelfId", id);
				responseMap.put("message", "User Shelf successfully created");
			
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in createUserShelfService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in createUserShelfService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from createUserShelfService : " + responseMap);
		return responseMap;
	}

	
	/**
	 * 
	 * Service method to get user shelf data
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param token
	 * @return
	 */
	public Map<String, Object> getUserShelfService(String shelfId, String token)
	{
		LOGGER.trace("in getUserShelfService... shelfId : " + shelfId);
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
			
				if(StringUtils.isEmpty(shelfId))
				{
					LOGGER.trace("No shelfId provided as request param. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "No shelfId provided as request param. Bad Request");
					return responseMap;
				}
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);
				
				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if(loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}
				
				// Get User shelf data
				ShelfDTO requiredShelf = shelfRepository.findById(shelfId);
				if(requiredShelf == null )
				{
					LOGGER.trace("No shelf data found associated to requested shelfId. Not Found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "No shelf data found associated to requested shelfId. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("requested shelf data : " + requiredShelf.toString());
				}
				
				// If logged-in user is shelfMaster, return the data
				if(requiredShelf.getShelfMaster().equals(loggedInUser.getId()))
				{
					LOGGER.trace("Shelf data is requested by the shelf master " + loggedInUser.getUsername() + ". Success");
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					responseMap.put("message", "Shelf data is requested by the shelf master. Success");
					responseMap.put("shelf", requiredShelf);
					return responseMap;
				}
				else if(requiredShelf.getShelfType().contains(ProfileConstants.USER_SHELF_PUBLIC))
				{
					LOGGER.trace("Shelf is a public shelf.Successful");
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					responseMap.put("message", "Shelf is a public shelf.Successful");
					responseMap.put("shelf", requiredShelf);
					return responseMap;
				}
				else if (requiredShelf.getShelfType().contains(ProfileConstants.USER_SHELF_PRIVATE)
						&& requiredShelf.getMembers().contains(loggedInUser.getId()))
				{
					LOGGER.trace("Shelf data is requested by shelf member " + loggedInUser.getUsername() + ". Success");
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					responseMap.put("message", "Shelf data is requested by shelf member. Success");
					responseMap.put("shelf", requiredShelf);
					return responseMap;
				}
				else
				{
					LOGGER.trace("User not allowed to view shelf data. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "User not allowed to view shelf data. Forbidden");
				}
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in createUserShelfService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getUserShelfService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from getUserShelfService : " + responseMap);
		return responseMap;
	}
	
	
	/**
	 * 
	 * Service method to get all user shelfs associated to a user
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 */
	public Map<String, Object> getAllUserShelfsService(String profileId, String token)
	{
		LOGGER.trace("in getAllUserShelfsService... profileId : " + profileId);
		Map<String, Object> responseMap = new HashMap<>();
		
		try 
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);
				
				// Get the user profile
				ProfileDTO loggedInUserprofile = profileRepository.findByUsername(username);
				if(loggedInUserprofile == null)
				{
					LOGGER.trace("Logged-in user profile not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Logged-in user profile not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user's profile : " + loggedInUserprofile.toString());
				}
				
				// Get requested user profile
				ProfileDTO requestedUserProfile = profileRepository.findById(profileId);
				if(requestedUserProfile == null)
				{
					LOGGER.trace("Profile associated to user not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Porfile associated to user not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Requested user's profile : " + requestedUserProfile.toString());
				}
				
				List<String> shelfIds = requestedUserProfile.getShelfs();
				LOGGER.trace("User shelfs associated to the profile : " + shelfIds);
				List<ShelfPostResponseDTO> shelfList = new ArrayList<>();
				ShelfDTO currentShelfDTO = new ShelfDTO();
				// if (loggedInUserprofile.getId().equals(requestedUserProfile.getId()))
				// Shelf master is requesting for all the shelfs
				if (loggedInUserprofile.getId().equals(profileId))
				{
					LOGGER.trace("The requested shelfs are owned by the loggedIn user");
					for (String eachShelfId : shelfIds)
					{
						ShelfPostResponseDTO currentPostResponseDto = new ShelfPostResponseDTO();
						LOGGER.trace("Current shelf id : " + eachShelfId);
						currentShelfDTO = shelfRepository.findById(eachShelfId);
						if (currentShelfDTO != null)
						{
							LOGGER.trace("Current shelf : " + currentShelfDTO.toString());
							currentPostResponseDto.setShelfId(currentShelfDTO.getId());
							currentPostResponseDto.setShelfName(currentShelfDTO.getShelfName());
							currentPostResponseDto.setShelfType(currentShelfDTO.getShelfType());
							currentPostResponseDto.setIsCollaborative(currentShelfDTO.getIsCollaborative());
							currentPostResponseDto.setIsPrivate(currentShelfDTO.getIsPrivate());
							currentPostResponseDto.setNoOfPosts(currentShelfDTO.getPosts().size());
							currentPostResponseDto.setCreatedDate(currentShelfDTO.getDate());
							currentPostResponseDto.setCollaborators(currentShelfDTO.getMembers());
							LOGGER.trace("current post response : " + currentPostResponseDto);
							shelfList.add(currentPostResponseDto);
						}
					}
					
					LOGGER.trace("List of shelfs : " + shelfList);
	  				responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					responseMap.put("message", "User shelfs fetched of user " + requestedUserProfile.getUsername() + " successfully");
					responseMap.put("shelfs", shelfList);
					
				}
				// Shelf members other than shelf master, are requesting shelfs data
				else
				{
					LOGGER.trace("Other than owner is requesting the shelfs");
					for (String eachShelfId : shelfIds)
					{
						ShelfPostResponseDTO currentPostResponseDto = new ShelfPostResponseDTO();
						LOGGER.trace("Current shelf id : " + eachShelfId);
						currentShelfDTO = shelfRepository.findById(eachShelfId);
						if(currentShelfDTO != null && currentShelfDTO.getShelfType().contains(ProfileConstants.USER_SHELF_PUBLIC))
						{
							LOGGER.trace("Current shelf is a public shelf : " + currentShelfDTO.toString());
							currentPostResponseDto.setShelfId(currentShelfDTO.getId());
							currentPostResponseDto.setShelfName(currentShelfDTO.getShelfName());
							currentPostResponseDto.setShelfType(currentShelfDTO.getShelfType());
							currentPostResponseDto.setIsCollaborative(currentShelfDTO.getIsCollaborative());
							currentPostResponseDto.setIsPrivate(currentShelfDTO.getIsPrivate());
							currentPostResponseDto.setNoOfPosts(currentShelfDTO.getPosts().size());
							currentPostResponseDto.setCreatedDate(currentShelfDTO.getDate());
							currentPostResponseDto.setCollaborators(currentShelfDTO.getMembers());
							LOGGER.trace("current post response : " + currentPostResponseDto);
							shelfList.add(currentPostResponseDto);
						}
						else if (currentShelfDTO != null
								&& currentShelfDTO.getMembers().contains(loggedInUserprofile.getId()))
						{
							LOGGER.trace("Current shelf : " + currentShelfDTO.toString());
							currentPostResponseDto.setShelfId(currentShelfDTO.getId());
							currentPostResponseDto.setShelfName(currentShelfDTO.getShelfName());
							currentPostResponseDto.setShelfType(currentShelfDTO.getShelfType());
							currentPostResponseDto.setIsCollaborative(currentShelfDTO.getIsCollaborative());
							currentPostResponseDto.setIsPrivate(currentShelfDTO.getIsPrivate());
							currentPostResponseDto.setNoOfPosts(currentShelfDTO.getPosts().size());
							currentPostResponseDto.setCreatedDate(currentShelfDTO.getDate());
							currentPostResponseDto.setCollaborators(currentShelfDTO.getMembers());
							LOGGER.trace("current post response : " + currentPostResponseDto);
							shelfList.add(currentPostResponseDto);
						}
					}
					
					LOGGER.trace("List of shelfs : " + shelfList);
	  				responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					responseMap.put("message", "User shelfs fetched successfully");
					responseMap.put("shelfs", shelfList);
					
				}
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in createUserShelfService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getUserShelfService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from getAllUserShelfsService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to delete requested userShelf
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param token
	 * @return
	 */
	public Map<String, Object> deleteUserShelfService(String shelfId, String token)
	{
		LOGGER.trace("in deleteUserShelfService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);
				
				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if(loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}
				
				// Check if the logged-in user has a profile or not
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("Logged-in user has no associated profile. Conflict");
					responseMap.put("statusCode", 409);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "Logged-in user has no associated profile. Conflict");
					return responseMap;
				}
				
				// Get logged in user's profile 
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged in user's profile : " + loggedInUserProfile.toString());
				
				ShelfDTO requiredShelf = shelfRepository.findById(shelfId);
				if(requiredShelf == null )
				{
					LOGGER.trace("No shelf data found associated to requested shelfId. Not Found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "No shelf data found associated to requested shelfId. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("requested shelf data : " + requiredShelf.toString());
				}
				
				// Cannot delete bookmarks shelf
				if(requiredShelf.getShelfName().equals(ProfileConstants.USER_BOOKMARKS_SHELF))
				{
					LOGGER.trace("Bookmarks shelf cannot be deleted. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Bookmarks shelf cannot be deleted. Forbidden");
					return responseMap;
				}
				
				if(!requiredShelf.getShelfMaster().equals(loggedInUserProfile.getId()))
				{
					LOGGER.trace("Delete action not triggered by shelf master. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Delete action not triggered by shelf master. Forbidden");
					return responseMap;
				}
				
				shelfRepository.deleteById(shelfId);
				LOGGER.trace("Deleted sheld data object from data table");
				
				loggedInUserProfile.getShelfs().remove(requiredShelf.getId());
				profileRepository.save(loggedInUserProfile);
				LOGGER.trace("Removed associated from user's profile");
				
				LOGGER.trace("Requested shelf data deleted successfully. Successful");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Requested shelf data deleted successfully. Successful");
					
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in deleteUserShelfService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in deleteUserShelfService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from deleteUserShelfService : " + responseMap);
		return responseMap;
	}

	
	/**
	 * 
	 * Service method to update shelf name if requested shelf
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param token
	 * @return
	 */
	public Map<String, Object> updateShelfNameService(String shelfId, String newShelfName, String token)
	{
		LOGGER.trace("in updateShelfNameService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);
				
				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if(loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}
				
				ShelfDTO requiredShelf = shelfRepository.findById(shelfId);
				if(requiredShelf == null )
				{
					LOGGER.trace("No shelf data found associated to requested shelfId. Not Found");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "No shelf data found associated to requested shelfId. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("requested shelf data : " + requiredShelf.toString());
				}
				
				// Check if the updateShelfName requested by shelf master
				if(!requiredShelf.getShelfMaster().equals(loggedInUser.getId()))
				{
					LOGGER.trace("The updateShelfName action is not triggered by shelf master. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "The updateShelfName action is not triggered by shelf master. Forbidden");
					return responseMap;
				}
				
				requiredShelf.setShelfName(newShelfName);
				requiredShelf.setUpdatedDate(profileServiceHelper.parseFormattedDateTime());
				LOGGER.trace("Update shelf data : " + requiredShelf.toString());
				shelfRepository.save(requiredShelf);
				
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "User shelf name updated successfully. Successful");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in updateShelfNameService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in updateShelfNameService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from updateShelfNameService : " + responseMap);
		return responseMap;
	}
	

	/**
	 * 
	 * Service method to pin posts to requesed user shelf
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> pinPostToShelfService(String shelfId, String postId, String token)
	{
		LOGGER.trace("in pinPostToShelfService... shelfId : " + shelfId + ", postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);
				
				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if(loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}
				
				// Get logged-in user's profile
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged-in user's profile : " + loggedInUserProfile.toString());
				
				if(!postConsumerService.isPostPresent(postId, token))
				{
					LOGGER.trace("Post associated to requested postId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Post associated to requested postId not found. Not Found");
					return responseMap;
				}
				
				ShelfDTO requiredShelf = shelfRepository.findById(shelfId);
				if(requiredShelf == null)
				{
					LOGGER.trace("User shelf associated to requested shelfId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "User shelf associated to requested shelfId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("required shelf : " + requiredShelf.toString());
				}
				
				PostData toBePinnedPost = new PostData();
				toBePinnedPost.setPostId(postId);
				toBePinnedPost.setAddedBy(loggedInUser.getId());
				toBePinnedPost.setDate(profileServiceHelper.parseFormattedDateTime());
				LOGGER.trace("To be pinned post data : " + toBePinnedPost.toString());
				
				if(requiredShelf.getShelfMaster().equals(loggedInUserProfile.getId()))
				{
					LOGGER.trace("Shelf master has requested the pin action");
					
					requiredShelf.getPosts().add(toBePinnedPost);
					shelfRepository.save(requiredShelf);
					LOGGER.trace("Post pinned to shelf and added to data table");
					
					LOGGER.trace("Logged in user is the shelf master and hence post pinned successfully. Successful");
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					responseMap.put("message", "Logged in user is the shelf master and hence post pinned successfully. Successful");
				}
				
				if(requiredShelf.getShelfType().contains(ProfileConstants.USER_SHELF_COLLABORATIVE) && requiredShelf.getMembers().contains(loggedInUserProfile.getId()))
				{
					LOGGER.trace("Shelf member has requested the pin action to a collaborative shelf");
					
					requiredShelf.getPosts().add(toBePinnedPost);
					shelfRepository.save(requiredShelf);
					LOGGER.trace("Post pinned to shelf and added to data table");
					
					LOGGER.trace("Logged in user is the collaborative shelf memeber and hence post pinned successfully. Successful");
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					responseMap.put("message", "Logged in user is the shelf master and hence post pinned successfully. Successful");
				}
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in pinPostToShelfService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in pinPostToShelfService : " + err);
			return responseMap;
		}
		LOGGER.trace("Response from pinPostToShelfService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to unpin posts to requesed user shelf
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param postId
	 * @param token
	 * @return
	 */
	public Map<String, Object> unpinPostToShelfService(String shelfId, String postId, String token)
	{
		LOGGER.trace("in unpinPostToShelfService... shelfId : " + shelfId + ", postId : " + postId);
		Map<String, Object> responseMap = new HashMap<>();

		try
		{
			if (authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");

				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// Get logged-in user's profile
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged-in user's profile : " + loggedInUserProfile.toString());
				
				// Check if the requested post is present or not
				if (!postConsumerService.isPostPresent(postId, token))
				{
					LOGGER.trace("Post associated to requested postId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Post associated to requested postId not found. Not Found");
					return responseMap;
				}

				// Get shelf associated to requested shelfId
				ShelfDTO requiredShelf = shelfRepository.findById(shelfId);
				if (requiredShelf == null)
				{
					LOGGER.trace("User shelf associated to requested shelfId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "User shelf associated to requested shelfId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("required shelf : " + requiredShelf.toString());
				}

				if (requiredShelf.getShelfMaster().equals(loggedInUserProfile.getId()))
				{
					LOGGER.trace("Shelf master has requested the unpin action");

					Optional<PostData> optionalPostData = requiredShelf.getPosts().stream()
							.filter(e -> e.getPostId().equals(postId)).findFirst();
					if (optionalPostData == null)
					{
						LOGGER.trace("Cannot find post pinned to shelf associated with requested postId. Not Found");
						responseMap.put("statusCode", 404);
						responseMap.put("statusMessage", "Not Found");
						responseMap.put("message",
								"Cannot find post pinned to shelf associated with requested postId. Not Found");
						return responseMap;
					}

					requiredShelf.getPosts().remove(optionalPostData.get());
					LOGGER.trace("Requested post unpinned from the user shelf");
					shelfRepository.save(requiredShelf);
					LOGGER.trace("Updated shelf data saved to shelf data table");

					LOGGER.trace("Logged in user is the shelf master and hence post unpinned from the shelf successfully. Successful");
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					responseMap.put("message",
							"Logged in user is the shelf master and hence post pinned successfully. Successful");
					return responseMap;
				}
				else if (requiredShelf.getMembers().contains(loggedInUserProfile.getId()))
				{
					LOGGER.trace("Shelf member has requested the unpin action");

					Optional<PostData> optionalPostData = requiredShelf.getPosts().stream()
							.filter(e -> e.getPostId().equals(postId)).findFirst();
					if (optionalPostData == null)
					{
						LOGGER.trace("Cannot find post pinned to shelf associated with requested postId. Not Found");
						responseMap.put("statusCode", 404);
						responseMap.put("statusMessage", "Not Found");
						responseMap.put("message",
								"Cannot find post pinned to shelf associated with requested postId. Not Found");
					}

					requiredShelf.getPosts().remove(optionalPostData.get());
					LOGGER.trace("Requested post unpinned from the user shelf");
					shelfRepository.save(requiredShelf);
					LOGGER.trace("Updated shelf data saved to shelf data table");

					LOGGER.trace("Logged in user is the shelf member and hence post pinned successfully. Successful");
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					responseMap.put("message",
							"Logged in user is the shelf member and hence post pinned successfully. Successful");
				}
				else
				{
					LOGGER.trace("User requesting unpin action is not authorized to perform the same. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message",
							"User requesting unpin action is not authorized to perform the same. Forbidden");
					return responseMap;
				}
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch (Exception err)
		{
			LOGGER.error("Error in unpinPostToShelfService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in unpinPostToShelfService : " + err);
			return responseMap;
		}

		LOGGER.trace("Response from unpinPostToShelfService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method for sending invitations for shelf collaborations to requested users
	 * 
	 * @author Akshay
	 * @param sendInvitationsForCollaborationRequestWrapper
	 * @param token
	 * @return
	 */
	public Map<String, Object> sendInvitationForCollaborationsService(
			SendInvitationsForCollaborationRequestWrapper sendInvitationsForCollaborationRequestWrapper, String token
	)
	{
		LOGGER.trace("in sendInvitationForCollaborationsService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				int totalUsersInvited = sendInvitationsForCollaborationRequestWrapper.getUsers().size();
				LOGGER.trace("total invited users : " + totalUsersInvited);
				
				int invitationsReceivedCount = 0;
				int disabledCollaborationsCount = 0;
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}
				
				// Check if the profile is associated to logged-in user
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No Profile found associated to logged in user. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No Profile found associated to logged in user. Forbidden");
					return responseMap;
				}
				
				// Get logged-in user's profile
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged-in user's profile : " + loggedInUserProfile.toString());
				
				// Get shelf associated to requested shelfId
				ShelfDTO requiredShelf = shelfRepository.findById(sendInvitationsForCollaborationRequestWrapper.getShelfId());
				if (requiredShelf == null)
				{
					LOGGER.trace("User shelf associated to requested shelfId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "User shelf associated to requested shelfId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("required shelf : " + requiredShelf.toString());
				}
				
				if(!requiredShelf.getShelfMaster().equals(loggedInUserProfile.getId()))
				{
					LOGGER.trace("Shelf master has not initiated the invitation request. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Shelf master has not initiated the invitation request. Forbidden");
					return responseMap;
				}
				
				// Get current date
				Date currentDate = profileServiceHelper.parseFormattedDateTime();
				LOGGER.trace("Current date : " + currentDate.toString());
				
				// Check if the invited user is a follower or not
				for(String eachInvitedUser : sendInvitationsForCollaborationRequestWrapper.getUsers())
				{
					LOGGER.trace("Current invited user : " + eachInvitedUser);
					Optional<Follower> possibleFollower = loggedInUserProfile.getFollowers().stream()
							.filter(e -> e.getProfileId().equals(eachInvitedUser)).findFirst();
					if(possibleFollower.isPresent())
					{
						LOGGER.trace(eachInvitedUser + " is present in user followers list");
						// Get invited user's profle
						ProfileDTO currentInvitedUserProfile = profileRepository.findById(eachInvitedUser);
						if(currentInvitedUserProfile.isCollaborationsAllowed())
						{
							LOGGER.trace("invited user has allowed collaborations");
							InvitationDTO currentInvitationRequest = new InvitationDTO();
							currentInvitationRequest.setDate(currentDate);
							currentInvitationRequest.setInvitedBy(loggedInUserProfile.getId());
							currentInvitationRequest.setShelfId(sendInvitationsForCollaborationRequestWrapper.getShelfId());
							currentInvitationRequest.setNoOfCollaboraters(requiredShelf.getMembers().size());
							LOGGER.trace("Invitation data object : " + currentInvitationRequest);
							if(currentInvitedUserProfile.getInvitations() != null)
							{
								LOGGER.trace("First invitations of the user");
								currentInvitedUserProfile.getInvitations().add(currentInvitationRequest);
							}
							else
							{
								List<InvitationDTO> tempInvitationsList = new ArrayList<>();
								tempInvitationsList.add(currentInvitationRequest);
								currentInvitedUserProfile.setInvitations(tempInvitationsList);
							}
							profileRepository.save(currentInvitedUserProfile);
							LOGGER.trace("Invitation request added to requested user's profile and data saved to data table");
							invitationsReceivedCount = invitationsReceivedCount + 1;
						}
						else
						{
							LOGGER.trace("Requested invitation user has disabled their collaborations");
							disabledCollaborationsCount = disabledCollaborationsCount + 1;
						}
					}
					else
					{
						LOGGER.trace(eachInvitedUser + " currently not present in followers list if logged-in user's profile");
					}
				}
				
				LOGGER.trace("Invitations successdfully received user count : " + invitationsReceivedCount);
				LOGGER.trace("Diabled collobations user count : " + disabledCollaborationsCount);
				if(invitationsReceivedCount == 0)
				{
					LOGGER.trace("All the requested users have disabled their collaborations. Successful");
					responseMap.put("message", "All the requested users have disabled their collaborations. Successful");
				}
				else if(invitationsReceivedCount == totalUsersInvited)
				{
					responseMap.put("message", "Successfully invitations to all the requested users. Successful");
				}
				else
				{
					responseMap.put("message", "Successfully invitations to many of the requested users. Successful");	
				}
				
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("totalUsersInvited", totalUsersInvited);
				responseMap.put("invitationsReceivedCount", invitationsReceivedCount);
				responseMap.put("disabledCollaborationsCount", disabledCollaborationsCount);
				
			}
			else 
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in sendInvitationForCollaborationsService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in sendInvitationForCollaborationsService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from sendInvitationForCollaborationsService : " + responseMap);
		return responseMap;
	}
	
	
	/**
	 * 
	 * Service method to accept all the pending invitations for collaborations
	 * 
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param token
	 * @return
	 */
	public Map<String, Object> acceptInvitationForCollaborationsService(String shelfId, String token)
	{
		LOGGER.trace("in acceptInvitationForCollaborationsService... shelfId : " + shelfId);
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}
				
				// Check if the profile is associated to logged-in user
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No Profile found associated to logged in user. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No Profile found associated to logged in user. Forbidden");
					return responseMap;
				}
				
				// Get logged-in user's profile
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged-in user's profile : " + loggedInUserProfile.toString());
				
				// Get shelf associated to requested shelfId
				ShelfDTO requiredShelf = shelfRepository.findById(shelfId);
				if (requiredShelf == null)
				{
					LOGGER.trace("User shelf associated to requested shelfId not found. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "User shelf associated to requested shelfId not found. Not Found");
					return responseMap;
				}
				else
				{
					LOGGER.trace("required shelf : " + requiredShelf.toString());
				}
				
				List<InvitationDTO> invitationsList = loggedInUserProfile.getInvitations();
				LOGGER.trace("Invitations list : " + invitationsList);
				if(invitationsList == null || invitationsList.size()==0)
				{
					LOGGER.trace("User profile has not pending invitations. Conflict");
					responseMap.put("statusCode", 409);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "User profile has not pending invitations. Conflict");
					return responseMap;
				}

				Optional<InvitationDTO> possibleRequiredShelfInvitation = invitationsList.stream().filter(e -> e.getShelfId().equals(shelfId)).findFirst();
				if(possibleRequiredShelfInvitation.isPresent())
				{
					LOGGER.trace("Requested shelf invitation is presnet");
					InvitationDTO requiredShelfInvitation = possibleRequiredShelfInvitation.get();
					LOGGER.trace("requested invitation data : " + requiredShelfInvitation);
					
					// Add logged-in user's profileId to shelf's member list and save shelf data
					requiredShelf.getMembers().add(loggedInUserProfile.getId());
					shelfRepository.save(requiredShelf);
					LOGGER.trace("User successfully added as a shelf member");
					
					// Remove the invitationDTO from profile's invitations list
					invitationsList.remove(requiredShelfInvitation);
					loggedInUserProfile.setInvitations(invitationsList);
					profileRepository.save(loggedInUserProfile);
					LOGGER.trace("User profile's data also updated accordingly");
					
					responseMap.put("statusCode", 200);
					responseMap.put("statusMessage", "Successful");
					responseMap.put("message", "User accepted requested invitation for collaboration. Successful");
					
				}
				else
				{
					LOGGER.trace("Requested shelf invitations not present. Not Found");
					responseMap.put("statusCode", 404);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "Requested shelf invitations not present. Not Found");
					return responseMap;
				}
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in acceptInvitationForCollaborationsService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in sendInvitationForCollaborationsService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from acceptInvitationForCollaborationsService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to toggle user profile's isCollaborationsAllowed
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 */
	public Map<String, Object> toggleCollaborationsService(String token)
	{
		LOGGER.trace("in toggleCollaborationsService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// Check if the profile is associated to logged-in user
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No Profile found associated to logged in user. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No Profile found associated to logged in user. Forbidden");
					return responseMap;
				}
				
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged in user profile : " + loggedInUserProfile.toString());
				
				LOGGER.trace("current collaboration permission : " + loggedInUserProfile.isCollaborationsAllowed());
				
				loggedInUserProfile.setCollaborationsAllowed(!loggedInUserProfile.isCollaborationsAllowed());
				loggedInUserProfile.setUpdatedDate(profileServiceHelper.parseFormattedDateTime());
				profileRepository.save(loggedInUserProfile);
				LOGGER.trace("Updated collaboration permission and saved data successfully. Successful");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Updated collaboration permission and saved data successfully. Successful");
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in toggleCollaborationsService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in sendInvitationForCollaborationsService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from toggleCollaborationsService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to get invitation count on requested user's profile
	 * 
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 */
	public int getInvitationCountService(String token)
	{
		LOGGER.trace("in getInvitationCountService");
		int invitationCount = 0;
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					return -2;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}
				
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No profile present for logged in user");
					return -3;
				}
				
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged in user profile : " + loggedInUserProfile.toString());
				
				if(loggedInUserProfile.getInvitations() == null || loggedInUserProfile.getInvitations().size() == 0)
				{
					invitationCount = 0;
				}
				else
				{
					invitationCount = loggedInUserProfile.getInvitations().size();
				}
				LOGGER.trace("invitations count : " + invitationCount);
			}
			else
			{
				LOGGER.trace("Unauthorized");
				invitationCount = -2;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getInvitationCountService : " + err);
			invitationCount = -1;
		}
		
		LOGGER.trace("Response from getInvitationCountService : " + invitationCount);
		return invitationCount;
	}

	
	/**
	 * 
	 * Service method to create user journal
	 * 
	 * @author Akshay
	 * @param createUserJournalRequestWrapper
	 * @param token
	 * @return
	 */
	public Map<String, Object> createUserJournalService(CreateUserJournalRequestWrapper createUserJournalRequestWrapper, String token)
	{
		LOGGER.trace("in createUserJournalService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Check if requested journal text is null or empty
				if(createUserJournalRequestWrapper.getJournalText()== null || createUserJournalRequestWrapper.getJournalText()== "")
				{
					LOGGER.trace("Requested journal text is null/empty. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "Requested journal text is null/empty. Bad Request");
					return responseMap;
				}
				
				// Validate chatacter length for the journal text
				if(createUserJournalRequestWrapper.getJournalText().length() > 14999)
				{
					LOGGER.trace("The character lenght for journal text exceed 14999 characters. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "The character lenght for journal text exceed 2999 characters. Forbidden");
					return responseMap;
				}
				
				// Validate if the journal date in the request is greater than current date
				if (createUserJournalRequestWrapper.getJournalDate()
						.compareTo(profileServiceHelper.parseFormattedDate()) > 0)
				{
					LOGGER.trace("Requested journal date is greater than current date. Conflict");
					responseMap.put("statusCode", 409);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "Requested journal date is greater than current date. Conflict");
					return responseMap;
				}
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// Check if the profile is associated to logged-in user
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No Profile found associated to logged in user. Precondition Failed");
					responseMap.put("statusCode", 412);
					responseMap.put("statusMessage", "Precondition Failed");
					responseMap.put("message", "No Profile found associated to logged in user. Precondition Failed");
					return responseMap;
				}
				
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged in user profile : " + loggedInUserProfile.toString());
				
				// Check if journal for same date already present
				JournalDTO possibleJournal = journalRepository.findByJournalDateAndProfile(createUserJournalRequestWrapper.getJournalDate(), loggedInUserProfile.getId());
				if(possibleJournal != null)
				{
					LOGGER.trace("Journal for the journal date : " + createUserJournalRequestWrapper.getJournalDate() + " already present. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "Journal for the journal date : " + createUserJournalRequestWrapper.getJournalDate() + " already present. Forbidden");
					return responseMap;
				}
				
				// Get current datetime
				Date currentDateTime = profileServiceHelper.parseFormattedDateTime();
				LOGGER.trace("Current date time : " + currentDateTime.toString());
				
				// Get current date
				Date currentDate = profileServiceHelper.parseFormattedDate();
				LOGGER.trace("Current date : " + currentDate.toString());
				
//				int daysDifferenceInDays = profileServiceHelper.getDifferenceInDays(createUserJournalRequestWrapper.getJournalDate(), currentDate);
//				LOGGER.trace("day difference between requested journal date and current date : " + daysDifferenceInDays + " days");
//				
//				if(daysDifferenceInDays > 30)
//				{
//					LOGGER.trace("The days difference is greated than 30 days. Forbidden");
//					responseMap.put("statusCode", 403);
//					responseMap.put("statusMessage", "Forbidden");
//					responseMap.put("message", "The days difference is greated than 30 days. Forbidden");
//					return responseMap;
//				}
				
				// Get year
				int year = profileServiceHelper.getYearFromDate(createUserJournalRequestWrapper.getJournalDate());
				LOGGER.trace("Journal year : " + year);
				
				// Get month
				String month = profileServiceHelper.getMonthFromDate(createUserJournalRequestWrapper.getJournalDate());
				LOGGER.trace("Journal month : " + month);
				
				// Get day
				int day = profileServiceHelper.getDayFromDate(createUserJournalRequestWrapper.getJournalDate());
				LOGGER.trace("Journal day of the month : " + day);
				
				if(currentDate.equals(createUserJournalRequestWrapper.getJournalDate()))
				{
					LOGGER.trace("User creating journal in present time");
				}
				else
				{
					LOGGER.trace("User creating journal from past time");
				}
				
				JournalDTO journal = new JournalDTO();
				UUID uuid = UUID.randomUUID();
				journal.setId("journal_" + uuid.toString());
				journal.setJournalText(createUserJournalRequestWrapper.getJournalText());
				journal.setJournalTitle(createUserJournalRequestWrapper.getJournalTitle());
				journal.setJournalDate(createUserJournalRequestWrapper.getJournalDate());
				journal.setProfileId(loggedInUserProfile.getId());
				journal.setDay(day);
				journal.setMonth(month);
				journal.setYear(year);
				journal.setPosted(false);
				journal.setDate(currentDateTime);
				journal.setUpdatedDate(currentDateTime);
				journalRepository.save(journal);
				LOGGER.trace("Journal date created and saved + " + journal.toString());
				
				responseMap.put("statusCode", 201);
				responseMap.put("statusMessage", "Created");
				responseMap.put("message", "User journal created and saved. Created");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in createUserJournalService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in createUserJournalService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from createUserJournalService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to get user journal by requested id
	 * 
	 * @author Akshay
	 * @param journalId
	 * @param token
	 * @return
	 */
	public Map<String, Object> getUserJournalByIdService(String journalId, String token)
	{
		LOGGER.trace("in getUserJournalByIdService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
					
				if(journalId == null || journalId == "")
				{
					LOGGER.trace("Requested journalId is null/empty. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "Requested journalId is null/empty. Bad Request");
					return responseMap;
				}
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// Check if the profile is associated to logged-in user
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No Profile found associated to logged in user. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No Profile found associated to logged in user. Forbidden");
					return responseMap;
				}
				
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged in user profile : " + loggedInUserProfile.toString());
				
				// Get the journal data
				JournalDTO requestedJournal = journalRepository.findByIdAndProfileId(journalId, loggedInUserProfile.getId());
				if(requestedJournal == null)
				{
					LOGGER.trace("No journal(s) associated to requested journalId and user's profile found. No Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "Not Found");
					responseMap.put("message", "No journal(s) associated to requested journalId and user's profile found. No Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Journal data associated to requested user's profile and journalId : " + requestedJournal.toString());
				}
				
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("journal", requestedJournal);
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getUserJournalByIdService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getUserJournalByIdService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from getUserJournalByIdService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to get user journals by requested year
	 * 
	 * @author Akshay
	 * @param year
	 * @param token
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public Map<String, Object> getUserJournalsByYearService(int year, String token)
	{
		LOGGER.trace("in getUserJournalsByYearService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				if(year > profileServiceHelper.parseFormattedDate().getYear() + 1900)
				{
					LOGGER.trace("Requested year is greater than current year. Conflict");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "Requested year is greater than current year. Conflict");
					return responseMap;
				}
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// Check if the profile is associated to logged-in user
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No Profile found associated to logged in user. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No Profile found associated to logged in user. Forbidden");
					return responseMap;
				}
				
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged in user profile : " + loggedInUserProfile.toString());
				
				List<JournalDTO> journals = journalRepository.findByYearAndProfileId(year, loggedInUserProfile.getId());
				if(journals == null || journals.size()==0)
				{
					LOGGER.trace("No journals found for requested user's profile and year. No Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "No journals found for requested user's profile and year. No Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("user journals : " + journals);
				}
				
				Collections.sort(journals, new Comparator<JournalDTO>()
				{
					public int compare(JournalDTO j1, JournalDTO j2)
					{
						return j1.getJournalDate().compareTo(j2.getJournalDate());
					}
				});
				LOGGER.trace("Sorted journals according to dates : " + journals);
				
				List<String> journalIds = new ArrayList<>();
				for(JournalDTO eachJournal : journals)
				{
					LOGGER.trace("current journal : " + eachJournal.toString());
					journalIds.add(eachJournal.getId());
				}
				
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("year", year);
				responseMap.put("journals", journalIds);
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch (Exception err) 
		{
			LOGGER.error("Error in getUserJournalByIdService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getUserJournalByIdService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from getUserJournalsByYearService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to get user journals by requested month
	 * 
	 * @author Akshay
	 * @param month
	 * @param token
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public Map<String, Object> getUserJournalsByMonthService(String month, int year,String token)
	{
		LOGGER.trace("in getUserJournalsByMonthService... month : " + month);
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				int monthInteger = profileServiceHelper.getMonthInteger(month);
				LOGGER.trace("Numberical month : " + monthInteger);
				if(monthInteger<0)
				{
					LOGGER.trace("Invalid requested month. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "Invalid requested month. Bad Request");
					return responseMap;
				}
				
				// LOGGER.trace("profileServiceHelper.parseFormattedDate().getMonth() : " + profileServiceHelper.parseFormattedDate().getMonth());
				if(year > profileServiceHelper.parseFormattedDate().getYear() + 1900 || monthInteger > profileServiceHelper.parseFormattedDate().getMonth())
				{
					LOGGER.trace("Requested month/year time is greater than current year. Conflict");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Conflict");
					responseMap.put("message", "Requested year is greater than current year. Conflict");
					return responseMap;
				}
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// Check if the profile is associated to logged-in user
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No Profile found associated to logged in user. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No Profile found associated to logged in user. Forbidden");
					return responseMap;
				}
				
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged in user profile : " + loggedInUserProfile.toString());
				
				List<JournalDTO> journals = journalRepository.findByMonthYearAndProfileId(month, year, loggedInUserProfile.getId());
				if(journals == null || journals.size()==0)
				{
					LOGGER.trace("No journals found for requested user's profile and year. No Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "No journals found for requested user's profile and year. No Content");
					return responseMap;
				}
				else
				{
					LOGGER.trace("user journals : " + journals);
				}
				
				Collections.sort(journals, new Comparator<JournalDTO>()
				{
					public int compare(JournalDTO j1, JournalDTO j2)
					{
						return j1.getJournalDate().compareTo(j2.getJournalDate());
					}
				});
				LOGGER.trace("Sorted journals according to dates : " + journals);
				
				List<String> journalIds = new ArrayList<>();
				for(JournalDTO eachJournal : journals)
				{
					LOGGER.trace("current journal : " + eachJournal.toString());
					journalIds.add(eachJournal.getId());
				}
				
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("month", month);
				responseMap.put("year", year);
				responseMap.put("journals", journalIds);
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in getUserJournalsByMonthService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in getUserJournalsByMonthService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from getUserJournalsByMonthService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to edit user journalText by journalId
	 * 
	 * @author Akshay
	 * @param editUserJournalRequestWrapper
	 * @param token
	 * @return
	 */
	public Map<String, Object> editUserJournalService(EditUserJournalRequestWrapper editUserJournalRequestWrapper, String token)
	{
		LOGGER.trace("in editUserJournalService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try 
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Check if requested journal text is null or empty
				if(editUserJournalRequestWrapper.getJournalText()== null || editUserJournalRequestWrapper.getJournalText()== "")
				{
					LOGGER.trace("Requested journal text is null/empty. Bad Request");
					responseMap.put("statusCode", 400);
					responseMap.put("statusMessage", "Bad Request");
					responseMap.put("message", "Requested journal text is null/empty. Bad Request");
					return responseMap;
				}
				
				// Validate character length for the journal text
				if(editUserJournalRequestWrapper.getJournalText().length() > 14999)
				{
					LOGGER.trace("The character lenght for journal text exceed 14999 characters. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "The character lenght for journal text exceed 2999 characters. Forbidden");
					return responseMap;
				}
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// Check if the profile is associated to logged-in user
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No Profile found associated to logged in user. Precondition Failed");
					responseMap.put("statusCode", 412);
					responseMap.put("statusMessage", "Precondition Failed");
					responseMap.put("message", "No Profile found associated to logged in user. Precondition Failed");
					return responseMap;
				}
				
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged in user profile : " + loggedInUserProfile.toString());
				
				// Get the requested journal data
				JournalDTO requestedJournal = journalRepository.findById(editUserJournalRequestWrapper.getJournalId());
				if(requestedJournal == null)
				{
					LOGGER.trace("Journal associated to requested journalId not found. No Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "Journal associated to requested journalId not found. No Content");
					return responseMap;
				}
				
				if(!requestedJournal.getProfileId().equals(loggedInUserProfile.getId()))
				{
					LOGGER.trace("The edit request is not trigged by the journal author. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "The edit request is not trigged by the journal author. Forbidden");
					return responseMap;
				}
				
				// Get current datetime
				Date currentDateTime = profileServiceHelper.parseFormattedDateTime();
				LOGGER.trace("Current date time : " + currentDateTime.toString());
				
				// Get current date
				Date currentDate = profileServiceHelper.parseFormattedDate();
				LOGGER.trace("Current date : " + currentDate.toString());
				
				// TO-DO date validation for more than 30 days
//				int daysDifferenceInDays = profileServiceHelper.getDifferenceInDays(requestedJournal.getJournalDate(), currentDate);
//				LOGGER.trace("day difference between requested journal date and current date : " + daysDifferenceInDays + " days");
//				
//				if(daysDifferenceInDays > 30)
//				{
//					LOGGER.trace("The days difference is greated than 30 days and hence cannot be edited. Forbidden");
//					responseMap.put("statusCode", 403);
//					responseMap.put("statusMessage", "Forbidden");
//					responseMap.put("message", "The days difference is greated than 30 days and hence cannot be edited. Forbidden");
//					return responseMap;
//				}
				
				requestedJournal.setJournalText(editUserJournalRequestWrapper.getJournalText());
				requestedJournal.setUpdatedDate(currentDateTime);
				journalRepository.save(requestedJournal);
				LOGGER.trace("Updated journal text and saved date to date table. Success");
				responseMap.put("statusCode", 200);
				responseMap.put("statusMessage", "Successful");
				responseMap.put("message", "Successfull updated journal text. Successful");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in editUserJournalService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in editUserJournalService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from editUserJournalService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Service method to post user journal
	 * 
	 * @author Akshay
	 * @param journalId
	 * @param token
	 * @return
	 */
	public Map<String, Object> postUserJournalService(PostUserJournalRequestWrapper postUserJournalRequestWrapper, String token)
	{
		LOGGER.trace("in postUserJournalService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user information from token
				String username = authConsumer.getUsernameFromToken(token.substring(7));
				LOGGER.trace("Logged in user's username : " + username);

				UserDTO loggedInUser = userConsumerService.getUserFromUsername(username, token);
				if (loggedInUser == null)
				{
					LOGGER.trace("No user found associated to username. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "No user found associated to username. Forbidden");
					return responseMap;
				}
				else
				{
					LOGGER.trace("Logged in user : " + loggedInUser.toString());
				}

				// Check if the profile is associated to logged-in user
				if(!loggedInUser.isProfilePresent())
				{
					LOGGER.trace("No Profile found associated to logged in user. Precondition Failed");
					responseMap.put("statusCode", 412);
					responseMap.put("statusMessage", "Precondition Failed");
					responseMap.put("message", "No Profile found associated to logged in user. Precondition Failed");
					return responseMap;
				}
				
				ProfileDTO loggedInUserProfile = profileRepository.findByUserId(loggedInUser.getId());
				LOGGER.trace("Logged in user profile : " + loggedInUserProfile.toString());
				
				// Get the requested journal data
				JournalDTO requestedJournal = journalRepository.findById(postUserJournalRequestWrapper.getJournalId());
				if(requestedJournal == null)
				{
					LOGGER.trace("Journal associated to requested journalId not found. No Content");
					responseMap.put("statusCode", 204);
					responseMap.put("statusMessage", "No Content");
					responseMap.put("message", "Journal associated to requested journalId not found. No Content");
					return responseMap;
				}
				
				if(!requestedJournal.getProfileId().equals(loggedInUserProfile.getId()))
				{
					LOGGER.trace("The edit request is not trigged by the journal author. Forbidden");
					responseMap.put("statusCode", 403);
					responseMap.put("statusMessage", "Forbidden");
					responseMap.put("message", "The edit request is not trigged by the journal author. Forbidden");
					return responseMap;
				}
				
				AddPostRequestWrapper addPostRequest = new AddPostRequestWrapper();
				addPostRequest.setPaged(false);
				addPostRequest.setPostType("writeup");
				addPostRequest.setText(requestedJournal.getJournalText());
				addPostRequest.setTitle(requestedJournal.getJournalTitle());
				addPostRequest.setHashtags(postUserJournalRequestWrapper.getHashtags());
				LOGGER.trace("data for posting user journal : " + addPostRequest.toString());
				responseMap = postConsumerService.addPostRequest(addPostRequest, token);
				
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in editUserJournalService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in editUserJournalService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from postUserJournalService : " + responseMap);
		return responseMap;
	}


	/**
	 * 
	 * Test Service to delete all shelfIds whose data not present
	 * 
	 * 
	 * @author Akshay
	 * @param profileId
	 * @param token
	 * @return
	 */
	public Map<String, Object> deleteAllUserShelfNotPresentService(String profileId, String token)
	{
		LOGGER.trace("in deleteAllUserShelfNotPresentService");
		Map<String, Object> responseMap = new HashMap<>();
		
		try
		{
			if(authConsumer.isTokenValid(token.substring(7)))
			{
				LOGGER.trace("Authorized");
				
				// Get user profile
				ProfileDTO profile = profileRepository.findById(profileId);
				LOGGER.trace("profile : " + profile.toString());
				
				List<String> validShelfIds = new ArrayList<>();
				List<String> presentShelfIds = profile.getShelfs();
				for(String eachShelfId : presentShelfIds)
				{
					LOGGER.trace("Current shelfId : " + eachShelfId);
					if(shelfRepository.findById(eachShelfId)!=null)
					{
						validShelfIds.add(eachShelfId);
					}
					
				}
				
				LOGGER.trace("valid shelf ids : " + validShelfIds);
				profile.setShelfs(validShelfIds);
				profileRepository.save(profile);
				LOGGER.trace("Successfully updated shelfId list to valid ids only");

				responseMap.put("statusCode", 200);
				responseMap.put("statusCode", "Successful");
				responseMap.put("message", "Successfully updated shelfId list to valid ids only");
			}
			else
			{
				LOGGER.trace("Unauthorized");
				responseMap.put("statusCode", 401);
				responseMap.put("statusMessage", "Unauthorized");
				responseMap.put("message", "User is not authorized to perform requested action");
				return responseMap;
			}
		}
		catch(Exception err)
		{
			LOGGER.error("Error in deleteAllUserShelfNotPresentService : " + err);
			responseMap.put("statusCode", 500);
			responseMap.put("statusMessage", "Server error");
			responseMap.put("message", "Error in editUserJournalService : " + err);
			return responseMap;
		}
		
		LOGGER.trace("Response from deleteAllUserShelfNotPresentService : " + responseMap);
		return responseMap;
	}


}