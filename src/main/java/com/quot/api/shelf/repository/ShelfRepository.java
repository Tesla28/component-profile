package com.quot.api.shelf.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.quot.api.dto.ShelfDTO;

@Repository
public interface ShelfRepository extends MongoRepository<ShelfDTO, Integer>
{

	ShelfDTO findById(String shelfId);

	void deleteById(String shelfId);

}
