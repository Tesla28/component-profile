package com.quot.api.constants;

public interface ProfileConstants
{
	public static final String VALUE_TYPE_PROFILE = "profile_";
	
	public static final String VALUE_TYPE_SHELF = "shelf_";
	
	// ************ User Shelf Constants ************ //
	
	public static final String USER_SHELF_INITIAL = "shelf";
	
	public static final String USER_SHELF_DEFAULT = "default";

	public static final String USER_SHELF_CUSTOM = "custom";
	
	public static final String USER_SHELF_PUBLIC = "public";
	
	public static final String USER_SHELF_PRIVATE = "private";
	
	public static final String USER_SHELF_COLLABORATIVE = "collaborative";
	
	public static final String USER_SHELF_NON_COLLABORATIVE = "noncollaborative";

	public static final String USER_BOOKMARKS_SHELF = "Bookmarks";

	// ********* Diary Constants ********* //
	
	public static final String YEAR_DIARY_TASK_EXECUTOR_BEAN = "YearDiaryTaskExecutor";

	public static final String YEAR_DIARY_THREAD_NAME_PREFIX = "userYearDiary-";

	// ********* Journal Post Type constant ********* //
	
	public static final String POST_TYPE_USER_JOURNAL = "user-journal";
	
}
