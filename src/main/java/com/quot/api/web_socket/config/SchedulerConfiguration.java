package com.quot.api.web_socket.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableScheduling
@Configuration
public class SchedulerConfiguration
{
	@Autowired
	SimpMessagingTemplate template;

//	@Scheduled(fixedDelay = 3000)
//	public void sendAdhocMessages()
//	{
//		template.convertAndSend("/quot/api/profile/get/invitations-count", new Object());
//	}
}
