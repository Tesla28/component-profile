package com.quot.api.web_socket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.quot.api.service.ProfileService;
import com.quot.api.web_socket.model.GetInvitationsCountWSResponse;

@Controller
public class WebSocketController
{
	
	@Autowired
	private ProfileService profileService;
	
	@MessageMapping("/ws/get-invitations-count")
    @SendTo("/topic/get-invitations-count")
    public GetInvitationsCountWSResponse GetInvitationsCountController() 
	{
        return new GetInvitationsCountWSResponse(profileService.getInvitationCountService(null));
    }
}
