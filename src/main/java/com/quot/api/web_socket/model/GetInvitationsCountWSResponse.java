package com.quot.api.web_socket.model;

public class GetInvitationsCountWSResponse
{
	private int invitationCount;
	
	public GetInvitationsCountWSResponse() 
	{
		
	}
	
	public GetInvitationsCountWSResponse(int invitationCount) 
	{
		this.invitationCount = invitationCount;
	}

	public int getInvitationCount()
	{
		return invitationCount;
	}

	public void setInvitationCount(int invitationCount)
	{
		this.invitationCount = invitationCount;
	}
	
}
