package com.quot.api.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.quot.api.auth.consumer.AuthConsumer;
import com.quot.api.diary.dto.CreateUserJournalRequestWrapper;
import com.quot.api.diary.dto.DiaryWrapperDTO;
import com.quot.api.diary.dto.EditUserJournalRequestWrapper;
import com.quot.api.diary.dto.PostUserJournalRequestWrapper;
import com.quot.api.dto.CreateUserShelfRequestWrapper;
import com.quot.api.dto.ProfileDTO;
import com.quot.api.dto.ProfileRequestDTO;
import com.quot.api.dto.UpdateBioRequestWrapper;
import com.quot.api.dto.UpdateInterestsRequestWrapper;
import com.quot.api.invitations.dto.SendInvitationsForCollaborationRequestWrapper;
import com.quot.api.scheduler.service.DiaryScheduler;
import com.quot.api.service.ProfileService;

@RestController
@RequestMapping("/quot/api/profile")
public class ProfileController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileController.class);
	
	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private AuthConsumer authConsumer;

	@Autowired
	private DiaryScheduler diaryScheduler;
	
	@SuppressWarnings("deprecation")
	@PostMapping(path = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<Map<String, Object>> createProfile(@RequestBody ProfileRequestDTO profileRequestDto,
			@RequestHeader("Authorization") String token)
	{
		LOGGER.trace("In Controller method of create profile... profileRequestDto : " + profileRequestDto);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.createProfileService(profileRequestDto, token);
		if (responseMap.get("statusCode").equals(new Integer(409)))
		{
			LOGGER.trace("Profile already present so CONFLICT creating a new profile");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
		else if (responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("User is forbidden from creating a profile");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("User is forbidden from creating a profile");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}

		LOGGER.trace("Response from createProfile controller : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CREATED);
	}

	
	/**
	 * Controller for getting profile data by user token, profileId or username
	 * @author Akshay
	 * @param profileId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getProfile(@RequestParam(name = "profileId", required = false) String profileId,
			@RequestParam(name = "username", required = false) String username,
			@RequestParam(name = "userId", required = false) String userId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in getProfile controller method... requested profileId : " + profileId + ", userName : " + username);
		Map<String, Object> responseMap = new HashMap<>();

		if(!authConsumer.isTokenValid(token.substring(7)))
		{
			LOGGER.trace("Unauthorized");
			responseMap.put("statusCode", 401);
			responseMap.put("statusMessage", "Unauthorized");
			responseMap.put("message", "User is not allowed to perform requested action");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED); 
		}
		
		ProfileDTO profile = new ProfileDTO();
		if(profileId!=null) 
		{
			LOGGER.trace("getting user profile by profileId");
			profile = profileService.getProfileById(profileId);
		}
		else if(userId != null)
		{
			LOGGER.trace("getting user profile by userId");
			profile = profileService.getProfileByUserId(userId);
		}
		else if(username!=null) 
		{
			LOGGER.trace("getting user profile by username");
			profile = profileService.getProfileByUserName(username);
		}
		else
		{
			LOGGER.trace("getting user profile by user token");
			profile = profileService.getProfileByUserTokenService(token);
		}
		
		if(profile == null)
		{
			LOGGER.trace("profile not found");
			responseMap.put("message", "No Profile present found");
			responseMap.put("statusCode", 404);
			responseMap.put("statusMessage", "Not Found");
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}

		responseMap.put("profile", profile);
		responseMap.put("statusMessage", "Successful");
		responseMap.put("statusCode", 200);
		
		LOGGER.trace("Response of getProfile : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * Controller for updating profile
	 * @author Akshay
	 * @param profileId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(path = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity updateProfile(@RequestBody ProfileDTO requestProfileDto,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in update profile controller method");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = profileService.udpdateProfile(requestProfileDto, token);
		if(responseMap.get("statusCode").equals(204))
		{
			LOGGER.trace("No Content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(500))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);	
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(401))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);	
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(403))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);	
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Success... responseMap : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * Controller method for updating user profile's bio
	 * 
	 * @author Akshay
	 * @param requestProfileDto
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/update/bio", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity updateProfileBio(@RequestBody UpdateBioRequestWrapper updateBioRequestWrapper,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in updateProfileBio controller method");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = profileService.updateProfileBioService(updateBioRequestWrapper, token);
		if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			LOGGER.trace("No Content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);	
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);	
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);	
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		
		LOGGER.trace("Response from updateProfileBio responseMap : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	/**
	 * 
	 * Controller method for updating user profile's interests
	 * 
	 * @param updateBioRequestWrapper
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "deprecation", "rawtypes" })
	@RequestMapping(path = "/update/interests", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity updateProfileInterests(@RequestBody UpdateInterestsRequestWrapper updateInterestsRequestWrapper,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in updateProfileInterests controller method");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = profileService.updateProfileInterestsService(updateInterestsRequestWrapper, token);
		if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			LOGGER.trace("No Content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);	
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);	
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);	
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		
		LOGGER.trace("Response from updateProfileInterests responseMap : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * Controller for following a user profile
	 * @author Akshay
	 * @param profileId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/follow", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity follow(@RequestParam(name = "followProfileId", required = true) String followProfileId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in follow profile controller method... followProfileId : " + followProfileId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = profileService.followService(followProfileId, token);
		if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			LOGGER.trace("Bad request... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("Bad request... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(409)))
		{
			LOGGER.trace("Bad request... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("Bad request... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		LOGGER.trace("Success... responseMap : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	/**
	 * 
	 * API to unfollow a user profile
	 * 
	 * @author Akshay
	 * @param profileFollowRequestDTO
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/unfollow", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity unfollow(@RequestParam(name = "followingProfileId", required = true) String followingProfileId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in follow profile controller method... followingProfileId : " + followingProfileId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.unfollowService(followingProfileId, token);
		if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			LOGGER.trace("Bad request... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("Bad request... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(409)))
		{
			LOGGER.trace("Bad request... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("Bad request... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		LOGGER.trace("Success... responseMap : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}

	/**
	 * 
	 * API for getting the multiple profile data in bulk
	 * 
	 * @author Akshay
	 * @param bulkProfilesRequestDto
	 * @param token
	 * @return Map<String, Object>
	 * @throws Exception
	 */
//	@Deprecated
//	@SuppressWarnings("rawtypes")
//	@RequestMapping(path = "/get/bulkProfiles", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity getBulkProfiles(@RequestBody BulkProfilesRequestDTO bulkProfilesRequestDto,
//			@RequestHeader("Authorization") String token) throws Exception
//	{
//		LOGGER.trace("in getBulkProfiles controller method... bulkProfilesRequestDto : " + bulkProfilesRequestDto);
//		Map<String, Object> responseMap = new HashMap<>();
//		
//		boolean isTokenValid = authConsumer.isTokenValid(token.substring(7));
//		if(!isTokenValid)
//		{
//			LOGGER.trace("Unauthorized");
//			responseMap.put("message", "Unauthorized");
//			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
//		}
//		
//		responseMap = profileService.getBulkProfilesDataService(bulkProfilesRequestDto);
//		if(responseMap.get("statusCode").equals(500))
//		{
//			LOGGER.trace("Server error... responseMap : " + responseMap);
//			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//		else if(responseMap.get("statusCode").equals(204))
//		{
//			LOGGER.trace("No content... responseMap : " + responseMap);
//			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
//		}
//		
//		LOGGER.trace("Success... responseMap : " + responseMap);
//		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
//	}
	
	/**
	 * 
	 * Controller method to get a users followings
	 * 
	 * @author Akshay
	 * @param bulkProfilesRequestDto
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(path = "/get/followings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getFollowings(@RequestParam(name = "profileId", required = true) String profileId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in getFollowings controller method... profileId : " + profileId);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = profileService.getFollowingService(profileId, token);
		if(responseMap.get("statusCode").equals(500))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(responseMap.get("statusCode").equals(401))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(204))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		
		LOGGER.trace("Success... responseMap : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	/**
	 * 
	 * Controller method to get followers of a user profile
	 * 
	 * @author Akshay
	 * @param profileId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/get/followers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getFollowers(@RequestParam(name = "profileId", required = true) String profileId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in getFollowers controller method... profileId : " + profileId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.getFollowersService(profileId, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(204)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		
		LOGGER.trace("Success... responseMap : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}	
	
	
	/**
	 * 
	 * API method to search a user profile
	 * 
	 * @author Akshay
	 * @param user
	 * @param token
	 * @return
	 * @throws Exception
	 */
//	@Deprecated
//	@SuppressWarnings("rawtypes")
//	@RequestMapping(path = "/search/profile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity searchUserProfile(@RequestParam(name = "username") String username,
//			@RequestParam(name = "searchString") String searchString, @RequestHeader("Authorization") String token)
//			throws Exception
//	{
//		LOGGER.trace("in searchUserProfile controller method... username : " + username);
//		Map<String, Object> responseMap = new HashMap<>();
//
//		responseMap = profileService.searchUserProfileService(username, searchString,token);
//		if(responseMap.get("statusCode").equals(new Integer(500)))
//		{
//			LOGGER.trace("Server error... responseMap : " + responseMap);
//			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//		else if(responseMap.get("statusCode").equals(new Integer(400)))
//		{
//			LOGGER.trace("No content... responseMap : " + responseMap);
//			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
//		}
//		else if(responseMap.get("statusCode").equals(new Integer(401)))
//		{
//			LOGGER.trace("No content... responseMap : " + responseMap);
//			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
//		}
//
//		LOGGER.trace("Success from searchUserProfile... responseMap : " + responseMap);
//		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
//	}
	
	/**
	 * 
	 * API to update username from user profile on username update from component-user
	 * 
	 * 
	 * @author Akshay
	 * @param currentUsername
	 * @param newUsername
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/update/profile/username", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity updateProfileUsername(@RequestParam(name = "currentUsername", required = true) String currentUsername,
			@RequestParam(name = "newUsername", required = true) String newUsername,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in updateProfileUsername controller method... currentUsername : " + currentUsername);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = profileService.updateProfileUsername(currentUsername, newUsername, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		
		LOGGER.trace("Success from updateProfileUsername... responseMap : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
	// ************************* User Shelf APIs ************************* //
	
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/shelf/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity createUserShelf(@RequestBody CreateUserShelfRequestWrapper createUserShelfRequestWrapper,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in createUserShelf controller method... createUserShelfRequestWrapper : "
				+ createUserShelfRequestWrapper.toString());
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.createUserShelfService(createUserShelfRequestWrapper, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Success from createUserShelf : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CREATED);
	}
	
	
	/**
	 * 
	 * API to get user shelf data
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/shelf/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getUserShelf(@RequestParam(name = "shelfId") String shelfId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in getUserShelf controller... shelfId : " + shelfId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.getUserShelfService(shelfId, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(400)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}
	
		LOGGER.trace("Success from getUserShelf : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to get all the shelf associated to a particular user
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/shelf/get-all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getAllUserShelfs(@RequestParam(name = "profileId")
	String profileId, @RequestHeader("Authorization")
	String token) throws Exception
	{
		LOGGER.trace("in getAllUserShelfs controller");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = profileService.getAllUserShelfsService(profileId, token);
		if (responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if (responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if (responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Success from getAllUserShelfs : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to delete user shelf from requested shelfId
	 * 
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/shelf/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity deleteUserShelf(@RequestParam(name = "shelfId") String shelfId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in deleteUserShelf controller... shelfId : " + shelfId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.deleteUserShelfService(shelfId, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Success from deleteUserShelf : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CREATED);
	}
	
	
	/**
	 * 
	 * Test API to delete all unwanted shelfIds saved in user profile
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping(path = "/shelf/delete-all-not-present", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity deleteAllUserShelfNotPresent(@RequestParam(name = "profileId") String profileId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in deleteAllUserShelfNotPresent controller... profileId : " + profileId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.deleteAllUserShelfNotPresentService(profileId, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		
		LOGGER.trace("Success from deleteAllUserShelfNotPresent : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	/**
	 * 
	 * API to update existing shelfName
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param newShelfName
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/shelf/update/name", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity updateShelfName(@RequestParam(name = "shelfId") String shelfId, @RequestParam(name = "newShelfName") String newShelfName,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in updateShelfName controller... shelfId : " + shelfId + ", newShelfName : " + newShelfName);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.updateShelfNameService(shelfId, newShelfName, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}

		LOGGER.trace("Success from deleteUserShelf : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CREATED);
	}
	
	
	// ************* Shelf Post actions ************* //
	
	
	/**
	 * 
	 * API to pin requested post to requested user shelf
	 * 
	 * @author Akshay
	 * @param shelfId
	 * @param postId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/shelf/post/pin", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity pinPostToShelf(@RequestParam(name = "shelfId") String shelfId, @RequestParam(name = "postId") String postId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in pinPostToShelf controller... shelfId : " + shelfId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.pinPostToShelfService(shelfId, postId, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(409)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
	
		LOGGER.trace("Success from pinPostToShelf : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CREATED);
	}
	
	
	/**
	 * 
	 * API to unpin requested post to requested user shelf
	 * 
	 * @param shelfId
	 * @param postId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/shelf/post/unpin", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity unpinPostToShelf(@RequestParam(name = "shelfId") String shelfId, @RequestParam(name = "postId") String postId,
			@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in unpinPostToShelf controller... shelfId : " + shelfId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.unpinPostToShelfService(shelfId, postId, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(409)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}
	
		LOGGER.trace("Success from unpinPostToShelf : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CREATED);
	}
	
	
	// ******************* Send and Respond Collaboration invitations ******************* //
	
	
	/**
	 * 
	 * API to send invitations to the requested users
	 * 
	 * 
	 * @author Akshay
	 * @param sendInvitationsForCollaborationRequestWrapper
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/shelf/invite-for-collaborations", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity sendInvitationForCollaborations(@RequestBody
	SendInvitationsForCollaborationRequestWrapper sendInvitationsForCollaborationRequestWrapper,
			@RequestHeader("Authorization")
			String token
	) throws Exception
	{
		LOGGER.trace("in sendInvitationForCollaborations controller... sendInvitationsForCollaborationRequestWrapper : "
				+ sendInvitationsForCollaborationRequestWrapper);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.sendInvitationForCollaborationsService(sendInvitationsForCollaborationRequestWrapper, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("Unauthorized... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("Not Found... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("Forbidden... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(409)))
		{
			LOGGER.trace("Conflict... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}

		LOGGER.trace("Success from sendInvitationForCollaborations : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
	
	/**
	 * 
	 * API to accept the pending invitation for collaboration
	 * 
	 * 
	 * @param shelfId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/shelf/accept-collaboration-request", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity acceptInvitationForCollaboration(@RequestParam(name = "shelfId")
	String shelfId, @RequestHeader("Authorization")
	String token) throws Exception
	{
		LOGGER.trace("in sendInvitationForCollaborations controller... shelfId : " + shelfId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.acceptInvitationForCollaborationsService(shelfId, token);
		if(responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if(responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("Unauthorized... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if(responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("Not Found... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if(responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("Forbidden... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if(responseMap.get("statusCode").equals(new Integer(409)))
		{
			LOGGER.trace("Conflict... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}

		LOGGER.trace("Success from sendInvitationForCollaborations : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
//	@SuppressWarnings({ "rawtypes", "deprecation" })
//	@RequestMapping(path = "/shelf/respond-for-collaborations", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity respondToInvitationForCollaborations(@RequestBody
//	SendInvitationsForCollaborationRequestWrapper sendInvitationsForCollaborationRequestWrapper,
//			@RequestHeader("Authorization")
//			String token
//	) throws Exception
//	{
//		LOGGER.trace("in respondToInvitationForCollaborations controller... sendInvitationsForCollaborationRequestWrapper : "
//				+ sendInvitationsForCollaborationRequestWrapper);
//		Map<String, Object> responseMap = new HashMap<>();
//	
//		
//		LOGGER.trace("Success from respondToInvitationForCollaborations : " + responseMap);
//		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
//	}
	
	
	/**
	 * 
	 * API to toggle isCollaborationsAllowed field from user's profile
	 * 
	 * @author Akshay
	 * @param sendInvitationsForCollaborationRequestWrapper
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/toggle-collaboration", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity toggleCollaborationsPermission(@RequestHeader("Authorization")
	String token) throws Exception
	{
		LOGGER.trace("in toggleCollaborationsPermission controller");
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = profileService.toggleCollaborationsService(token);
		if (responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if (responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if (responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if (responseMap.get("statusCode").equals(new Integer(409)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}

		LOGGER.trace("Success from toggleCollaborationsPermission : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	/**
	 * 
	 * API to get invitation count for an user
	 * 
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path = "/get/invitations-count", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public int getInvitationCounts(@RequestHeader("Authorization") String token) throws Exception
	{
		LOGGER.trace("in getInvitationCounts controller");
		int invitationCounts = 0;
		
		invitationCounts = profileService.getInvitationCountService(token);
		if(invitationCounts == -1)
		{
			LOGGER.error("Error in getting the count");
		}
		else if(invitationCounts == -2)
		{
			LOGGER.error("User is unauthorized");
		}
		else if(invitationCounts == -3)
		{
			LOGGER.error("Forbidden");
		}
		
		LOGGER.trace("Success from getInvitationCounts : " + invitationCounts);
		// return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
		return invitationCounts;
	}
	
	
	
	// ************** Schduler Test ************** //
	
	/**
	 * 
	 * API to test Year Diary Scheduler
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path = "/scheduler/test/year-diary", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public CompletableFuture<List<ProfileDTO>> testYearDiaryScheduler() throws Exception
	{
		LOGGER.trace("in testYearDiaryScheduler controller");
		
		return diaryScheduler.getAllUserProfiles();
	}
	
	
	/**
	 * 
	 * API to test Year Diary Scheduler
	 * 
	 * @author Akshay
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path = "/scheduler/test/get-diary-wrapper", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public CompletableFuture<List<DiaryWrapperDTO>> testGetDiaryWrapperDTOScheduler() throws Exception
	{
		LOGGER.trace("in testGetDiaryWrapperDTOScheduler controller");
		return diaryScheduler.getDiaryWrapperFuture();
	}
	
	
	// ************** User Diary APIs ************** //
	
	
	/**
	 * 
	 * API to create a user journal
	 * 
	 * 
	 * @author Akshay
	 * @param createUserJournalRequestWrapper
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/journal/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity createUserJournal(@RequestBody CreateUserJournalRequestWrapper createUserJournalRequestWrapper, @RequestHeader(name = "Authorization") String token) throws Exception
	{
		LOGGER.trace("in createUserJournal controller... createUserJournalRequestWrapper : " + createUserJournalRequestWrapper.toString());
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.createUserJournalService(createUserJournalRequestWrapper, token);
		if (responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if (responseMap.get("statusCode").equals(new Integer(403)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.FORBIDDEN);
		}
		else if (responseMap.get("statusCode").equals(new Integer(412)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.PRECONDITION_FAILED);
		}
		else if (responseMap.get("statusCode").equals(new Integer(409)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CONFLICT);
		}

		LOGGER.trace("Success from createUserJournal : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.CREATED);
	}
	
	
	/**
	 * 
	 * API to get user journal data by requested id
	 * 
	 * @author Akshay
	 * @param journalId
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/journal/get-by-id", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getUserJournalById(@RequestParam(required = true, name = "journalId") String journalId, @RequestHeader(name = "Authorization") String token) throws Exception
	{
		LOGGER.trace("in getUserJournalById controller... journalId : " + journalId);
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.getUserJournalByIdService(journalId, token);
		if (responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if (responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if (responseMap.get("statusCode").equals(new Integer(204)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		
		LOGGER.trace("Success from getUserJournalById : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to get user journals filters by year
	 * 
	 * @author Akshay
	 * @param year
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/journals/get-by-year", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getUserJournalsByYear(@RequestParam(required = true, name = "year") int year, @RequestHeader(name = "Authorization") String token) throws Exception
	{
		LOGGER.trace("in getUserJournalsByYear controller... year : " + year);
		Map<String, Object> responseMap = new HashMap<>();
	
		responseMap = profileService.getUserJournalsByYearService(year, token);
		if (responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if (responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if (responseMap.get("statusCode").equals(new Integer(204)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}
		
		LOGGER.trace("Success from getUserJournalsByYear : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to get user journals filters by month
	 * 
	 * @author Akshay
	 * @param year
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/journals/get-by-month", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getUserJournalsByMonth(@RequestParam(required = true, name = "month") String month, 
			@RequestParam(required = true, name = "year") int year, 
			@RequestHeader(name = "Authorization")
	String token) throws Exception
	{
		LOGGER.trace("in getUserJournalsByMonth controller... month : " + month +  ", year : " + year);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = profileService.getUserJournalsByMonthService(month, year,token);
		if (responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		else if (responseMap.get("statusCode").equals(new Integer(404)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NOT_FOUND);
		}
		else if (responseMap.get("statusCode").equals(new Integer(204)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.NO_CONTENT);
		}

		LOGGER.trace("Success from getUserJournalsByMonth : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
	
	
	/**
	 * 
	 * API to edit a journalText by journalId
	 * 
	 * @author Akshay
	 * @param month
	 * @param year
	 * @param token
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/journal/edit", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity editUserJournal(@RequestBody EditUserJournalRequestWrapper editUserJournalRequestWrapper, @RequestHeader(name = "Authorization")
	String token) throws Exception
	{
		LOGGER.trace("in editUserJournal controller... editUserJournalRequestWrapper : " + editUserJournalRequestWrapper.toString());
		Map<String, Object> responseMap = new HashMap<>();
		
		responseMap = profileService.editUserJournalService(editUserJournalRequestWrapper, token);
		if (responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		
		LOGGER.trace("Success from editUserJournal : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);	
	}
	
	
	@SuppressWarnings({ "rawtypes", "deprecation" })
	@RequestMapping(path = "/journal/post", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity postUserJournal(@RequestBody PostUserJournalRequestWrapper postUserJournalRequestWrapper, @RequestHeader(name = "Authorization")
	String token) throws Exception
	{
		LOGGER.trace("in postUserJournal controller... postUserJournalRequestWrapper : " + postUserJournalRequestWrapper);
		Map<String, Object> responseMap = new HashMap<>();

		responseMap = profileService.postUserJournalService(postUserJournalRequestWrapper, token);
		if (responseMap.get("statusCode").equals(new Integer(500)))
		{
			LOGGER.trace("Server error... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else if (responseMap.get("statusCode").equals(new Integer(401)))
		{
			LOGGER.trace("No content... responseMap : " + responseMap);
			return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.UNAUTHORIZED);
		}
		
		LOGGER.trace("Success from postUserJournal : " + responseMap);
		return new ResponseEntity<Map<String, Object>>(responseMap, HttpStatus.OK);
	}
}
